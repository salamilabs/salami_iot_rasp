import asyncio
import json
import logging
import socket

import aioredis
import tornado.httpserver
import tornado.web
import tornado.websocket
from tornado import gen
from tornado.platform.asyncio import AsyncIOMainLoop

TORNADO_PORT = 8888

AsyncIOMainLoop().install()
loop = asyncio.get_event_loop()

'''
TO DO

Add log information to track number of istances of the WSUpstream and WSDownstream CLASS 
using the connect method to track the connected client ID 

Add CHANNEL routing to a specific SalamiKitchen (querystring in the connect ?)

SOLVE TASk WAS DESTROYED BUT IT IS PENDING line 127
'''


class WSUpstreamHandler(tornado.websocket.WebSocketHandler):
    def __init__(self, app, request, **kwargs):
        super().__init__(app, request, **kwargs)
        self._logger = logging.getLogger(self.__class__.__name__)
        self._message_counter = 0
        self._redis_pub_conn = None
        self._redis_ready = False
        self._websocket_client_id = "UNKNOWN"
        self._ready = False

    @gen.coroutine
    def redis_connection(self):
        self._redis_pub_conn = yield from aioredis.create_redis(('localhost', 6379), loop=loop)
        self._logger.info('Redis connection for PUBLISH done')
        self._redis_ready = True

    def print_request_details(self):
        self._logger.info("Request: %s", self.request)
        self._logger.info("Request type: %s", type(self.request))
        self._logger.info("Request remote ip: %s", self.request.remote_ip)

    def open(self):
        try:
            self._logger.info('New connection to Upstream')
            self._websocket_client_id = self.request.query_arguments["id"][0].decode('UTF-8')
            self._logger.info('Websocket client id: %s', self._websocket_client_id)
            self._ready = True
        except Exception:
            self._logger.exception("Error in connection: ")
            self._ready = False
            self.close()

    def data_received(self, chunk):
        pass

    @gen.coroutine
    def publish(self, message):

        if not self._redis_ready:
            yield self.redis_connection()
        if self._redis_ready:
            try:
                self._logger.info("Publishing to channel: %s", str(message["destination"]))
                res = yield from self._redis_pub_conn.publish_json('channel:' + str(message["destination"]), message)
                self._logger.debug("Active subscribers of channel: %s are %s", str(message["destination"]), str(res))
            except Exception:
                self._logger.exception("Error occurred while publishing ")

    @gen.coroutine
    def on_message(self, message):
        if self._ready:
            salami_message = json.loads(message)
            yield self.publish(salami_message)

    def on_close(self):
        if self._redis_pub_conn is not None:
            self._redis_pub_conn.close()
        self._logger.info('Upstream connection closed')

    def check_origin(self, origin):
        return True


class WSDownstreamHandler(tornado.websocket.WebSocketHandler):
    def __init__(self, app, request, **kwargs):
        super().__init__(app, request, **kwargs)
        self._logger = logging.getLogger(self.__class__.__name__)
        self._message_counter = 0

        self._task_SELF = None

        self._channel_SELF = None

        self._myredis = None
        self._subscribed = False

        self._websocket_client_id = "UNKNOWN"
        self._ready = False
        self._redis_ready = False

    @asyncio.coroutine
    def redis_connect(self):
        self._myredis = yield from aioredis.create_redis(('localhost', 6379), loop=loop)
        self._redis_ready = True

    @asyncio.coroutine
    def subscribe_channels(self):
        if self._redis_ready:

            ch = yield from self._myredis.subscribe('channel:' + str(self._websocket_client_id))
            self._channel_SELF = ch[0]
            self._logger.info('Channel Subscribed: %s', str(self._channel_SELF.name))

            self._subscribed = True

    @asyncio.coroutine
    def async_reader(self, channel):
        while self._subscribed:
            msg = yield from channel.get(encoding='utf-8')
            self.write_message(msg)

    @gen.coroutine
    def open(self):
        try:
            self._logger.info('New connection to Downstream')
            self._websocket_client_id = self.request.query_arguments["id"][0].decode('UTF-8')
            self._logger.info('Websocket client id: %s', self._websocket_client_id)

            self._logger.info('Connecting to REDIS')
            yield from self.redis_connect()
            self._logger.info('Subscribing CHANNELS')
            yield from self.subscribe_channels()

            self._task_SELF = asyncio.async(self.async_reader(self._channel_SELF))

        except Exception:
            self._logger.exception("Error in connection: ")
            self._ready = False
            self.close()

    @gen.coroutine
    def on_message(self, message):
        pass

    def data_received(self, chunk):
        pass

    @gen.coroutine
    def on_close(self):
        if self._myredis is not None:
            try:
                self._subscribed = False
                self._logger.info("WSHandler for WSClient[ %s ] is unsubscribing from CHANNELS...",
                                  self._websocket_client_id)

                yield from self._myredis.unsubscribe('channel:' + self._websocket_client_id)

                self._myredis.close()
            except Exception:
                self._logger.exception("Error occurred while unsubscribing from channels ")

        self._logger.info('Downstream connection closed')

    def check_origin(self, origin):
        return True


class WSSystemHandler(tornado.websocket.WebSocketHandler):
    def __init__(self, app, request, **kwargs):
        super().__init__(app, request, **kwargs)
        self._logger = logging.getLogger(self.__class__.__name__)
        self._message_counter = 0

        self._task_ALL = None
        self._channel_ALL = None

        self._subscribe_redis = None
        self._publish_redis = None
        self._subscribed = False

        self._websocket_client_id = "UNKNOWN"
        self._redis_publish_ready = False
        self._redis_subscribe_ready = False

    @asyncio.coroutine
    def redis_subscribe_connect(self):
        self._subscribe_redis = yield from aioredis.create_redis(('localhost', 6379), loop=loop)
        self._redis_subscribe_ready = True

    @asyncio.coroutine
    def redis_publish_connect(self):
        self._publish_redis = yield from aioredis.create_redis(('localhost', 6379), loop=loop)
        self._redis_publish_ready = True

    @asyncio.coroutine
    def subscribe_channel(self):
        if self._redis_subscribe_ready:
            ch = yield from self._subscribe_redis.subscribe('channel:ALL')
            self._channel_ALL = ch[0]
            self._logger.info('Channel Subscribed: %s', str(self._channel_ALL.name))

            self._subscribed = True

    @asyncio.coroutine
    def async_reader(self, channel):
        while self._subscribed:
            msg = yield from channel.get(encoding='utf-8')
            self.write_message(msg)

    @gen.coroutine
    def publish(self, message):
        if not self._redis_publish_ready:
            yield self.redis_publish_connect()
        if self._redis_publish_ready:
            try:
                self._logger.info("Publishing to channel: %s", str(message["destination"]))
                res = yield from self._publish_redis.publish_json('channel:' + str(message["destination"]), message)
                self._logger.debug("Active subscribers of channel: %s are %s", str(message["destination"]), str(res))
            except Exception:
                self._logger.exception("Error occurred while publishing ")

    @gen.coroutine
    def open(self):
        try:
            self._logger.info('New connection to SystemStream')
            self._websocket_client_id = self.request.query_arguments["id"][0].decode('UTF-8')
            self._logger.info('Websocket client id: %s', self._websocket_client_id)

            self._logger.info('Connecting to REDIS')
            yield from self.redis_subscribe_connect()
            yield from self.redis_publish_connect()
            self._logger.info('Subscribing CHANNEL')
            yield from self.subscribe_channel()

            self._task_ALL = asyncio.async(self.async_reader(self._channel_ALL))

        except Exception:
            self._logger.exception("Error in connection: ")
            self._redis_publish_ready = False
            self._redis_subscribe_ready = False
            self.close()

    @gen.coroutine
    def on_message(self, message):
        if self._redis_publish_ready:
            salami_message = json.loads(message)
            yield self.publish(salami_message)

    def data_received(self, chunk):
        pass

    @gen.coroutine
    def on_close(self):
        if self._subscribe_redis is not None:
            try:
                self._subscribed = False
                self._logger.info("WSHandler for WSClient[ %s ] is unsubscribing from CHANNELS...",
                                  self._websocket_client_id)
                yield from self._subscribe_redis.unsubscribe('channel:ALL')

                self._subscribe_redis.close()
            except Exception:
                self._logger.exception("Error occurred while unsubscribing from channels ")

        if self._redis_publish_ready is not None:
            try:
                self._publish_redis.close()
            except Exception:
                self._logger.exception("Error occurred while closing redis publish connection")

        self._logger.info('SystemStream connection closed')

    def check_origin(self, origin):
        return True


application = tornado.web.Application([
    (r'/upstream', WSUpstreamHandler), (r'/downstream', WSDownstreamHandler), (r'/system', WSSystemHandler),
])

if __name__ == "__main__":
    log_format = "%(levelname) -10s %(asctime)s %(module)s:%(lineno)s %(funcName)s %(message)s"
    logging.basicConfig(format=log_format, level=logging.INFO, filename="tornado.log")
    logger = logging.getLogger(__name__)
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(TORNADO_PORT)
    myIP = socket.gethostbyname(socket.gethostname())
    logger.info('*** Websocket Server Started at %s, on port %s ***', myIP, TORNADO_PORT)
    loop.run_forever()
