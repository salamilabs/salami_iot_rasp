import logging

from kitchen import IOTKitchen
from model.assistantchef import SalamiInitDataBuilder
from passport import SalamiPassport
from receiver import MulticastReceiver


class ABSSlice:
    def __init__(self, slice_slot):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._slice_slot = slice_slot
        self._passport = SalamiPassport()
        self._iot = None

    def activate(self):
        MulticastReceiver().block_until_message("HIS_MASTERS_VOICE")
        init_data = SalamiInitDataBuilder().with_slice_slot(self._slice_slot).with_toi_url(
            self._passport.toi_url).with_socket_url(self._passport.socket_url).with_message_callback(
            self.on_message).adapt_to().object()
        self._iot = IOTKitchen(init_data)
        try:
            self._iot.connect()
        except Exception:
            self._logger.exception("An error occurred: ")
            self._iot.disconnect()

    def deactivate(self):
        self._iot.disconnect()

    def on_message(self, message):
        pass

    def send_ingredient_update(self):
        pass
