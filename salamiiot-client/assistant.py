import logging

import pykka

import SalamiConstants
from communication.httpclient import HttpClient
from communication.subscriber import SalamiSubscriber
from decorators import sleep_after, consumes
from model.masterchef import SalamiMasterChefInfo
from model.message import SalamiMessageBuilder, SalamiMessage
from model.recipe import SalamiRecipe
from model.util import SalamiModelUtil
from worker import ChefWorker


class SalamiAssistantChef(pykka.ThreadingActor):
    def __init__(self, init_data):
        super(SalamiAssistantChef, self).__init__()
        self._logger = logging.getLogger(self.__class__.__name__)

        self._slot = init_data.slice_slot

        self._master_chef_info = SalamiMasterChefInfo()

        self._publisher = None
        self._receiver = None
        self._system = None

        self._toi_url = init_data.toi_url
        self._socket_url = init_data.socket_url
        self._ingredients = []

        self._worker = None

        self._kitchen_callback = init_data.message_callback

    @sleep_after(.1)
    def _join_kitchen(self, socket_url):
        self._receiver = SalamiSubscriber(socket_url + "/downstream?id=" + str(self._slot), self.on_downstream_message)
        self._receiver.start()

        self._publisher = SalamiSubscriber(socket_url + "/upstream?id=" + str(self._slot), self.on_upstream_message)
        self._publisher.start()

        self._system = SalamiSubscriber(socket_url + "/system?id=ALL", self.on_system_message)
        self._system.start()

    def _retrieve_recipe(self, toi_url):
        recipe_url = toi_url + SalamiConstants.RECIPE_URL
        self._logger.info("Retrieving recipe from %s", recipe_url)
        http_client = HttpClient(recipe_url)
        recipe = http_client.get().object(SalamiRecipe)
        return recipe

    def _load_ingredients(self, recipe):
        self._logger.info("Loading ingredients for slice %s", self._slot)
        self._ingredients.clear()

        for ingredient in recipe.ingredients:
            if ingredient.address == self._slot:
                self._logger.debug("Found relevant ingredient: %s", ingredient.ingredient_name)

                ingredient_class = ingredient.category
                if ingredient_class == SalamiConstants.DATA_INGREDIENT_CLASS:
                    behavior = ingredient.behavior
                    if behavior == SalamiConstants.DATA_BEHAVIOR_ON_REQUEST:
                        self._logger.debug("Found pollable ingredient: %s", ingredient.name)
                        ingredient.pollable = True

                self._ingredients.append(ingredient)

    def _send_status_update(self, status):
        self._logger.info("Changing status to %s", status)

        builder = SalamiMessageBuilder()
        builder.with_source(self._slot)
        builder.with_destination("ALL")
        builder.with_ingredient("")
        builder.with_command(SalamiConstants.CMD_ADVERTISE_SLICE)
        builder.with_command_data({"id": self._slot, "status": status})
        message = builder.adapt_to().json()
        self._logger.debug("Sending status change message %s", message)

        self._system.publish(message)

    def _validate_message(self, message):
        result = False
        if str(message.source) != str(self._slot) and message.destination == "ALL":
            result = True
        elif message.destination == self._slot:
            result = True
        return result

    def set_kitchen_callback(self, callback):
        self._kitchen_callback = callback

    def on_receive(self, command):
        self._logger.debug("Command is %s", command)

        if SalamiConstants.JOIN_KITCHEN in command:
            self._join_kitchen(self._socket_url)
            self._send_status_update("INITIALIZING")

            self._send_status_update("RETRIEVING RECIPE")
            recipe = self._retrieve_recipe(self._toi_url)
            self._send_status_update("LOADING INGREDIENTS")
            self._load_ingredients(recipe)

            self._worker = ChefWorker(self._ingredients)
            self._worker.set_to_slice_message_callback(self.on_action_message)
            self._send_status_update("RUNNING")

            return True

        if SalamiConstants.UPDATE_INGREDIENT in command:
            message = command[SalamiConstants.UPDATE_INGREDIENT]
            command_data = message.command_data
            name = command_data["name"]
            value = command_data["value"]
            self._logger.info("Received ingredient update command: Name %s, Value %s", name, value)

            self._logger.debug("Message received from managed slice %s", command)

            if message.command == SalamiConstants.CMD_SEND_DATA_EVENT_AC:
                ingredient_name = message.ingredient
                ingredient = SalamiModelUtil.find_one_by_name(self._ingredients, ingredient_name)
                if ingredient:
                    self._logger.info("Ingredient %s is a recipe's relevant ingredient", ingredient_name)

                    message = SalamiMessageBuilder().with_source(self._slot).with_destination(
                        self._master_chef_info.id).with_ingredient(ingredient.ingredient_index).with_command(
                        SalamiConstants.CMD_SEND_DATA_EVENT_AC).with_command_data(
                        command_data).adapt_to().json()
                    self._publisher.publish(message)
                else:
                    self._logger.info("Update received refers to a non relevant ingredient: %s", ingredient_name)
            else:
                self._logger.error("Message received is not a CMD_SEND_DATA_EVENT_AC as expected")

    @consumes(SalamiMessage)
    def on_downstream_message(self, message):
        self._logger.info("Received downstream message: %s", message)
        if self._validate_message(message):
            self._worker.process(message)

    def on_upstream_message(self, message):
        self._logger.info("Received upstream message: %s", message)

    @consumes(SalamiMessage)
    def on_system_message(self, message):
        self._logger.info("Received System Message: %s", message)

        if message.command == SalamiConstants.CMD_REFRESH_ALL:
            self._logger.info("Refreshing RECIPE!")
            self._send_status_update("REFRESHING RECIPE")
            recipe = self._retrieve_recipe(self._toi_url)
            self._send_status_update("RELOADING INGREDIENTS")

            self._load_ingredients(recipe)
            self._send_status_update("RUNNING")

    def on_action_message(self, message):
        self._kitchen_callback(message)
