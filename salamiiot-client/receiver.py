import json
import logging
import socket
import struct

import SalamiConstants
from communication.subscriber import SalamiSubscriber
from exception import InvalidApiKeyException
from model.message import SalamiMessageBuilder
from security import SecurityManager
from toi import ToiParser
from utils import MessageCallbackPlaceholder

MCAST_GRP = '224.1.1.1'
MCAST_PORT = 5007


class MulticastReceiver:
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._socket.bind(('', MCAST_PORT))
        mreq = struct.pack("4sl", socket.inet_aton(MCAST_GRP), socket.INADDR_ANY)
        self._socket.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        self._security_manager = SecurityManager()
        self._publisher = None

    def block_until_message(self, message_key):
        self._logger.info("Waiting for Multicast broadcast")
        while True:
            json_message = self._socket.recv(10240).decode("UTF-8")
            message = json.JSONDecoder().decode(json_message)
            self._logger.debug("Received message from MasterChef: %s", message)
            if message_key in message:
                # Save Key and broker address
                message_content = message["HIS_MASTERS_VOICE"]
                self._security_manager.save_key(message_content["KEY"])
                if self._security_manager.check_key():
                    parser = ToiParser("/opt/salamiiot/data/BUTTON.slice")
                    toi_dict = parser.parse()
                    socket_url = message_content['BROKER_URL'] + "/system?id=ALL"
                    self._publisher = SalamiSubscriber(socket_url, MessageCallbackPlaceholder)
                    self._publisher.start()
                    join_message = {
                        "slot": 1,
                        "toi": toi_dict
                    }
                    builder = SalamiMessageBuilder()
                    builder.with_source("1")
                    builder.with_destination("ALL")
                    builder.with_ingredient("")
                    builder.with_command(SalamiConstants.CMD_ADVERTISE_SLICE)
                    builder.with_command_data(join_message)
                    salami_message = builder.adapt_to().json()
                    self._publisher.publish(salami_message)
                else:
                    raise InvalidApiKeyException("Error - ApiKey not present or invalid")
                exit(0)
