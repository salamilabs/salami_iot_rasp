import SalamiConstants
from assistant import SalamiAssistantChef
from model.message import SalamiMessageBuilder

JOIN_COMMAND = {SalamiConstants.JOIN_KITCHEN: 'JOIN'}


class IOTKitchen:
    def __init__(self, init_data):
        self._init_data = init_data
        self._joined = False

        self._assistant_chef = None

    def connect(self):
        self._assistant_chef = SalamiAssistantChef.start(init_data=self._init_data)
        self._joined = self._assistant_chef.ask(JOIN_COMMAND)

    def update_ingredient(self, command, name, value):
        builder = SalamiMessageBuilder()
        builder.with_ingredient(name)
        builder.with_command(command)
        builder.with_command_data({'name': name, 'value': value})
        message = builder.adapt_to().object()

        self._assistant_chef.tell({SalamiConstants.UPDATE_INGREDIENT: message})

    def disconnect(self):
        self._assistant_chef.stop()
