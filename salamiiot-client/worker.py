import logging

import SalamiConstants
from model.action import SalamiActionBuilder
from utils import MessageCallbackPlaceholder


class ChefWorker:
    def __init__(self, ingredients):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._ingredients = ingredients
        self._to_slice_message_callback = MessageCallbackPlaceholder

    def set_to_slice_message_callback(self, callback):
        self._to_slice_message_callback = callback

    def process(self, message):
        # only relevant and correct messages arrive at this point.
        self._logger.info("Processing command with code: %s", str(message.command))
        received_command = message.command

        if (received_command == SalamiConstants.CMD_DO_GPIO_ACTION) or (
                    received_command == SalamiConstants.CMD_DO_MESSAGE_ACTION):
            action_builder = SalamiActionBuilder()
            if received_command == SalamiConstants.CMD_DO_GPIO_ACTION:
                self._logger.info("GPIO ACTION requested: %s", message)
                action_builder.of_type(SalamiConstants.GPIO_ACTION_TYPE)
            if received_command == SalamiConstants.CMD_DO_MESSAGE_ACTION:
                action_builder.of_type(SalamiConstants.MESSAGE_ACTION_TYPE)
                self._logger.info("GPIO ACTION requested: %s", message)

            action_builder.with_name(self.decode_ingredient_name(message.ingredient))
            action_builder.with_dataload(message.command_data)
            action = action_builder.adapt_to().json()

            self._logger.info("Converted to SalamiAction: %s", action)
            self._to_slice_message_callback(action)

    def decode_ingredient_name(self, ingredient_id):
        for ingredient in self._ingredients:
            if ingredient.ingredient_index == ingredient_id:
                self._logger.debug("Decoding : %s = %s", str(ingredient_id), ingredient)
                return ingredient.ingredient_name
