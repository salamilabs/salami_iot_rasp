import xml.etree.ElementTree


class ToiParser:
    def __init__(self, toi_path):
        self.toi_path = toi_path
        self._toi_object = {}

    def parse(self):
        slice_dom = xml.etree.ElementTree.parse(self.toi_path).getroot()
        self._parse_slice(slice_dom)
        return self._toi_object

    def _parse_slice(self, slice_element):
        name = slice_element.get("name")
        version = slice_element.get("version")
        author = slice_element.get("author")

        self._toi_object['name'] = name
        self._toi_object['version'] = version
        self._toi_object['author'] = author

        ingredients_dom = slice_element.find('ingredients')
        self._parse_ingredients(ingredients_dom)

    def _parse_ingredients(self, ingredients_element):
        self._toi_object['ingredients'] = {}
        data_dom = ingredients_element.find('data')
        self._parse_data(data_dom)
        events_dom = ingredients_element.find('events')
        self._parse_events(events_dom)
        actions_dom = ingredients_element.find('actions')
        self._parse_actions(actions_dom)

    def _parse_data(self, data_element):
        self._toi_object['ingredients']['data'] = []
        datum_list = data_element.findall('datum')
        for datum in datum_list:
            dict_datum = {'name': datum.get('name'), 'code': datum.get('code'), 'description': datum.get('description'),
                          'class': datum.get('class'), 'behavior': datum.get('behavior'), 'type': datum.get('type'),
                          'input_pattern': datum.get('input_pattern'), 'updatable': datum.get('updatable')}
            self._toi_object['ingredients']['data'].append(dict_datum)

    def _parse_events(self, events_elements):
        self._toi_object['ingredients']['events'] = []
        event_list = events_elements.findall("event")
        for event in event_list:
            dict_event = {'name': event.get('name'), 'code': event.get('code'), 'description': event.get('description'),
                          'class': event.get('class'), 'type': event.get('type')}
            self._toi_object['ingredients']['events'].append(dict_event)

    def _parse_actions(self, actions_element):
        self._toi_object['ingredients']['actions'] = []
        action_list = actions_element.findall("action")
        for action in action_list:
            dict_action = {'name': action.get('name'), 'code': action.get('code'),
                           'description': action.get('description'), 'class': action.get('class'),
                           'gpio_mask': action.get('gpio_mask'), 'type': action.get('type')}
            self._toi_object['ingredients']['actions'].append(dict_action)
