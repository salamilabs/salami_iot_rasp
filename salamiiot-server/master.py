import pykka

import SalamiConstants
from advertiser import MulticastAdvertiser
from communication.httpclient import HttpClient
from communication.subscriber import SalamiSubscriber
from decorators import sleep_after, consumes
from logger import SalamiLogger
from model.masterchef import SalamiMasterChefInfo
from model.message import SalamiMessageBuilder, SalamiMessage
from model.recipe import SalamiRecipe
from model.task import SalamiTaskDataBuilder
from model.util import SalamiModelUtil
from task import SalamiTask


class SalamiMasterChef(pykka.ThreadingActor):
    def __init__(self, init_data):
        super(SalamiMasterChef, self).__init__()
        self._toi_url = init_data.toi_url
        self._socket_url = init_data.socket_url

        self._logger = SalamiLogger(self._socket_url).get_logger(SalamiConstants.MASTERCHEF_LOGGER_ID)
        self._info = SalamiMasterChefInfo()

        self._receiver = None
        self._system = None

        self._recipe = None
        self._ingredients = []
        self._tasks = []

        self._clock = None

        self._multicast_advertiser = MulticastAdvertiser()

        self._bootstrap()

    def _bootstrap(self):
        self._join_kitchen(self._socket_url)
        self._send_status_update("INITIALIZING")
        self._send_status_update("RETRIEVING RECIPE")
        self._recipe = self._retrieve_recipe(self._toi_url)
        self._send_status_update("LOADING INGREDIENTS")
        self._load_ingredients()
        self._send_status_update("CREATING TASKS")
        self._create_tasks()
        self._send_status_update("RUNNING")
        self._multicast_advertiser.start()

    def _process_incoming_message(self, json_message):
        message = SalamiModelUtil.json_to_object(json_message, SalamiMessage)
        command = message.command
        if command == SalamiConstants.CMD_SEND_DATA_EVENT_AC or command == SalamiConstants.CMD_REFRESH_ALL:
            self._dispatch_to_tasks(message)
        else:
            self._logger.warn("The message is not relevant")

    def _dispatch_to_tasks(self, message):
        self._logger.debug("Dispatching to tasks %s", message)

        for task in self._tasks:
            task.tell({'salami_message': message})

    @sleep_after(.1)
    def _join_kitchen(self, socket_url):
        self._receiver = SalamiSubscriber(socket_url + "/downstream?id=" + str(self._info.id),
                                          self.on_downstream_message)
        self._receiver.start()

        self._system = SalamiSubscriber(socket_url + "/system?id=ALL", self.on_system_message)
        self._system.start()

    def _retrieve_recipe(self, toi_url):
        recipe_url = toi_url + "/SalamiTOIKitchen/recipe/my.recipe"
        self._logger.info("Retrieving recipe from %s", recipe_url)
        http_client = HttpClient(recipe_url)
        recipe = http_client.get().object(SalamiRecipe)
        return recipe

    def _load_ingredients(self):
        self._ingredients = self._recipe.ingredients

    def _send_status_update(self, status):
        self._logger.info("Changing status to %s", status)

        builder = SalamiMessageBuilder()
        builder.with_source(self._info.id)
        builder.with_destination("ALL")
        builder.with_ingredient("")
        builder.with_command(SalamiConstants.CMD_ADVERTISE_MC)
        builder.with_command_data({"id": self._info.id, "status": status})
        message = builder.adapt_to().json()
        self._logger.debug("Sending status change message %s", message)

        self._system.publish(message)

    def on_downstream_message(self, message):
        self._logger.info("Received downstream message: %s", message)
        self._process_incoming_message(message)

    def on_upstream_message(self, message):
        self._logger.info("Received upstream message: %s", message)

    @consumes(SalamiMessage)
    def on_system_message(self, message):
        self._logger.info("Received system message: %s", message)
        if message.command == SalamiConstants.CMD_REFRESH_ALL:
            self._logger.info("Requested recipe refresh")

            for task in self._tasks:
                try:
                    del task
                except Exception:
                    self._logger.exception("Error while stopping the Task")

            self._tasks.clear()
            self._receiver.disconnect()
            self._system.disconnect()
            self._bootstrap()

    def _create_tasks(self):
        for i, step in enumerate(self._recipe.steps, start=0):
            self._logger.info("Creating new task with id: %s", str(i))
            builder = SalamiTaskDataBuilder()
            builder.with_task_id(i)
            builder.with_ingredients(self._ingredients)
            builder.with_masterchef_id(self._info.id)
            builder.with_toi_url(self._toi_url)
            builder.with_socket_url(self._socket_url)
            builder.with_timeframe(self._recipe.timeframe)
            builder.with_step(step)
            task_data = builder.adapt_to().object()
            task = SalamiTask.start(task_data)
            self._tasks.append(task)

    def on_receive(self, message):
        pass
