import socket
import threading

import time

from configuration import ConfigurationManager
from model.util import SalamiModelUtil


class MulticastAdvertiser(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self._configuration = ConfigurationManager()
        self._multicast_group = self._configuration.get_configuration_entry("multicast_group")
        self._multicast_port = self._configuration.get_configuration_entry("multicast_port")
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self._socket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)

    def _send(self, message):
        json_message = SalamiModelUtil.object_to_json(message)
        self._socket.sendto(bytes(json_message, "UTF-8"), (self._multicast_group, self._multicast_port))

    def run(self):
        while True:
            message = {
                "HIS_MASTERS_VOICE": {"KEY": "a5c72aee7047975751324e56cb2df8f8", "BROKER_URL": "ws://127.0.0.1:8888"}}
            self._send(message)
            time.sleep(10)
