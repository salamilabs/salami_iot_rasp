import logging
import threading
import time

import SalamiConstants


class SalamiClock(threading.Thread):
    def __init__(self, callback):
        self.__logger = logging.getLogger(self.__class__.__name__)
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self.active = True
        self.__clock_rate = SalamiConstants.CLOCK_RATE

        self.__subscriber = callback

    def run(self):
        while True and self.active:
            self.__subscriber()
            time.sleep(self.__clock_rate)
        self.__logger.info("Bye bye!")

    def stop_ticker(self):
        self.__logger.info("Stopping the ticker")
        self.active = False
