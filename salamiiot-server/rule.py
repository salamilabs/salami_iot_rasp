import logging

import SalamiConstants


class SalamiRule:
    def __init__(self, rule_data):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._ingredient_index = rule_data.ingredient_index
        self._condition = rule_data.condition
        self._checks_chain = []

        if self._condition.check_value == 1:
            self._logger.info("FIXED CHECK CONDITION")
            if self._condition.value1 != "":
                self._logger.debug("Added: %s", str(self._condition.value1))
                self._add_atomic_check(SalamiAtomicCheck(SalamiConstants.CHECK_TYPE_EQ, self._condition.value1))
            if self._condition.value2 != "":
                self._logger.debug("Added: %s", str(self._condition.value2))
                self._add_atomic_check(SalamiAtomicCheck(SalamiConstants.CHECK_TYPE_EQ, self._condition.value2))
            if self._condition.value3 != "":
                self._logger.debug("Added: %s", str(self._condition.value3))
                self._add_atomic_check(SalamiAtomicCheck(SalamiConstants.CHECK_TYPE_EQ, self._condition.value3))

        if self._condition.check_interval == 1:
            self._logger.info("INTERVAL CHECK CONDITION")
            if self._condition.min != "":
                self._logger.debug("Added: %s", str(self._condition.min))
                self._add_atomic_check(SalamiAtomicCheck(SalamiConstants.CHECK_TYPE_GT, self._condition.min))
            if self._condition.max != "":
                self._logger.debug("Added: %s", str(self._condition.max))
                self._add_atomic_check(SalamiAtomicCheck(SalamiConstants.CHECK_TYPE_LT, self._condition.max))

    def _add_atomic_check(self, atomic_check):
        self._checks_chain.append(atomic_check)

    def is_unchained(self):
        return self._condition.unchained == 1

    def is_inertial(self):
        return self._condition.inertial == 1

    def __str__(self):
        result = ""
        result = result + "Ingredient:" + str(self._ingredient_index["ingredient_index"])
        result = result + " (" + self._ingredient_index["slice"]
        result = result + "." + self._ingredient_index["ingredient_name"] + ")"
        result = result + "Inertial:" + str(self.is_inertial()) + " "
        result = result + "Unchained:" + str(self.is_unchained()) + " "
        result += "Check ["
        for item in self._checks_chain:
            symbol = ''
            if item.check_type == SalamiConstants.CHECK_TYPE_EQ:
                symbol = "="
            if item.check_type == SalamiConstants.CHECK_TYPE_GT:
                symbol = ">"
            if item.check_type == SalamiConstants.CHECK_TYPE_LT:
                symbol = "<"
            result = result + "value" + symbol + str(item.value)
        result += "]"
        return result

    def check(self, value):
        if self._condition.check_value == 1:  # EXIT AS SOON AS A CONDITION IS VERIFIED (at least ONE should be TRUE)
            for atomic_check in self._checks_chain:
                return atomic_check.check(value)

        if self._condition.check_interval == 1:
            # EXIT AS SOON AS AN ATOMIC CONDITION IS NOT VERIFIED (all should be TRUE)
            for atomic_check in self._checks_chain:
                return atomic_check.check(value)


class SalamiAtomicCheck:
    def __init__(self, check_type, value):
        self.__logger = logging.getLogger(self.__class__.__name__)
        self.__check_type = check_type
        self.__value = value

    def check(self, value):
        if self.__check_type == SalamiConstants.CHECK_TYPE_EQ:
            self.__logger.debug("Checking " + str(value) + " EQUAL value " + str(self.__value))
            return str(self.__value) == str(value)
        if self.__check_type == SalamiConstants.CHECK_TYPE_GT:
            self.__logger.debug("Checking " + str(value) + " GREATER THAN " + str(self.__value))
            return value > float(self.__value)
        if self.__check_type == SalamiConstants.CHECK_TYPE_LT:
            self.__logger.debug("Checking " + str(value) + " LESS THAN " + str(self.__value))
            return value < float(self.__value)
        return False
