import datetime


class SalamiCrontab:
    def __init__(self, crontab):
        self.days = []
        self.minutes = []
        self.hours = []
        self.months = []
        self.weekdays = []
        self.always = False
        self.active = False
        self.crontab = crontab

        if crontab == "":
            self.active = False
        else:

            self.active = True
            self.always = (crontab == "*|*|*|*|*|")

            elements = str(crontab).split("|")
            self.minutes = elements[0].split(",")
            self.hours = elements[1].split(",")
            self.days = elements[2].split(",")
            self.months = elements[3].split(",")
            self.weekdays = elements[4].split(",")

    def check(self):
        date = datetime.datetime.now()
        actual_minute = str(date.minute)
        actual_hour = str(date.hour)
        actual_day = str(date.day)
        actual_month = str(date.month)
        actual_weekday = date.weekday()
        if actual_weekday == 6:  # python starts from MONDAY crontab from SUNDAY
            actual_weekday = 0
        else:
            actual_weekday += 1
        actual_weekday = str(actual_weekday)

        if self.minutes.__contains__("*") or self.minutes.__contains__(actual_minute):
            # print self.minutes,"--> minute",date.minute,"ok"
            pass
        else:
            return False

        if self.hours.__contains__("*") or self.hours.__contains__(actual_hour):
            # print self.hours,"--> hour",date.hour,"ok"
            pass
        else:
            # print self.hours,"--> hour",date.hour,"FAIL"
            return False

        if self.days.__contains__("*") or self.days.__contains__(actual_day):
            # print self.days,"--> day",actual_day,"ok"
            pass
        else:
            # print self.days,"--> day",actual_day,"FAIL"
            return False

        if self.months.__contains__("*") or self.months.__contains__(actual_month):
            # print self.months,"--> month",actual_month,"ok"
            pass
        else:
            # print self.months,"--> month",actual_month,"FAIL"
            return False

        if self.weekdays.__contains__("*") or self.weekdays.__contains__(actual_weekday):
            # print self.weekdays,"--> weekday",actual_weekday,"ok"
            pass
        else:
            # print self.weekdays,"--> weekday",actual_weekday,"FAIL"
            return False
        return True
