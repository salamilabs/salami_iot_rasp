import SalamiConstants
from exception import InvalidApiKeyException, InvalidPassportException
from master import SalamiMasterChef
from model.assistantchef import SalamiInitDataBuilder
from security import SecurityManager, MASTERCHEF_PASSPORT_TYPE


class Bootstrap:
    def __init__(self):
        self._security_manager = SecurityManager()

    def init(self):
        self._bootstrap_masterchef()

    def _bootstrap_masterchef(self):
        if self._security_manager.check_key():
            if self._security_manager.check_passport(MASTERCHEF_PASSPORT_TYPE):
                socket_url = self._security_manager.get_passport_entry(SalamiConstants.SOCKET_URL)
                toi_url = self._security_manager.get_passport_entry(SalamiConstants.TOI_URL)
                master_init_data = SalamiInitDataBuilder().with_socket_url(socket_url).with_toi_url(
                    toi_url).adapt_to().object()
                SalamiMasterChef.start(master_init_data)
            else:
                raise InvalidPassportException("Error - Invalid Masterchef Passport")
        else:
            raise InvalidApiKeyException("Error - ApiKey not present or invalid")


if __name__ == "__main__":
    bootstrap = Bootstrap()
    bootstrap.init()
