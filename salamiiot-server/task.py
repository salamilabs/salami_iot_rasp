import logging
import time

import pykka

import SalamiConstants
from communication.subscriber import SalamiSubscriber
from crontab import SalamiCrontab
from model.message import SalamiMessageBuilder
from model.rule import SalamiRuleDataBuilder
from model.task import SalamiTaskCounter, SalamiTaskReportBuilder
from model.task import SalamiTransactionInfo
from model.util import SalamiModelUtil
from rule import SalamiRule
from ticker import SalamiClock
from utils import MessageCallbackPlaceholder, get_current_milliseconds_time


class SalamiTask(pykka.ThreadingActor):
    def __init__(self, task_data):
        super(SalamiTask, self).__init__()
        self._logger = logging.getLogger(self.__class__.__name__)
        self._task_data = task_data

        self._publisher = None

        self._crontab = SalamiCrontab(self._task_data.step.crontab)
        self._task_counter = SalamiTaskCounter()
        self._transaction_info = SalamiTransactionInfo()
        self._sleep_time = task_data.step.sleep
        self._sleeping = False

        self._rule_checker_registry = {}

        self._actions = []
        self._rules = {}

        self._ticker = None

        self._bootstrap()

    def _add_rule(self, condition):

        ingredient_index = condition.ingredient_index
        self._logger.info("Processing condition for ingredient: %s - %s", str(condition), str(ingredient_index))
        ingredients = self._task_data.ingredients
        ingredient = SalamiModelUtil.find_one_by_index(ingredients, ingredient_index)

        if ingredient:
            if ingredient_index in self._rules:
                self._logger.warn("A rule already exists for this ingredient: %s", str(ingredient_index))
            else:
                self._logger.info("Creating new rule for ingredient: %s", ingredient.name)
                rule_data = SalamiRuleDataBuilder().with_ingredient_index(ingredient_index).with_condition(
                    condition).adapt_to().object()
                rule = SalamiRule(rule_data)
                self._rules[ingredient_index] = rule
                self._rule_checker_registry[ingredient_index] = False
        else:
            self._logger.error("No ingredient found for this rule")
            self.stop()

    def _add_action(self, action):
        ingredient_index = action.ingredient_index
        self._logger.info("Processing action for ingredient: %s - %s", str(action), str(ingredient_index))

        ingredients = self._task_data.ingredients
        ingredient = SalamiModelUtil.find_one_by_index(ingredients, ingredient_index)
        if ingredient:
            destination = ingredient.address
            action_type = ingredient.ingredient_type

            self._logger.info("Creating new action for ingredient %s", ingredient)
            builder = SalamiMessageBuilder()
            builder.with_source(self._task_data.masterchef_id)
            builder.with_destination(destination)
            builder.with_ingredient(ingredient_index)
            builder.with_command(SalamiModelUtil.get_action_command(action_type))
            builder.with_command_data(action.dataload)
            action_request = builder.adapt_to().object()

            self._actions.append(action_request)
            self._logger.debug("Adding action: %s", action_request)
        else:
            self._logger.error("No ingredient found for this action")
            self.stop()

    def _evaluate_rules(self, ingredient_index, ingredient_value):
        self._logger.info("Checking ingredient of index: %s whit value: %s", str(ingredient_index), str(
            ingredient_value))
        self._logger.debug("EvaluateConditions: %s", self._rules[ingredient_index])
        if (self._rules[ingredient_index].is_inertial()) and (
                    self._rule_checker_registry[ingredient_index] == True):
            self._logger.warn("This update refers to an INERTIAL CONDITION already checked. Nothing to do.")
            return

        if self._rules[ingredient_index].check(ingredient_value):
            self._mark_condition(ingredient_index, SalamiConstants.CONDITION_CHECKED)
        else:
            self._mark_condition(ingredient_index, SalamiConstants.CONDITION_NOT_CHECKED)

    def _reset_task_work(self):
        self._reset_rule_checker_registry()
        self._transaction_info.start_time = 0

    def _bootstrap(self):
        self._publisher = SalamiSubscriber(self._task_data.socket_url + "/upstream?id=" + self._task_data.masterchef_id,
                                           MessageCallbackPlaceholder)
        self._publisher.start()
        step = self._task_data.step
        for condition in step.conditions:
            self._add_rule(condition)

        for action in step.actions:
            self._add_action(action)

        if self._crontab.active:
            self._rule_checker_registry["CRONTAB"] = False
        self._reset_task_work()

        self._ticker = SalamiClock(self.on_clock_tick)
        self._ticker.start()

    def on_receive(self, message):
        self._logger.info("Started TASK at %s", str(get_current_milliseconds_time()))
        task_report = None
        max_timeframe = self._task_data.timeframe * 100

        if not self._sleeping:

            if self._transaction_info.start_time != 0:
                elapsed_time = get_current_milliseconds_time() - self._transaction_info.start_time
                if elapsed_time > max_timeframe:
                    self._logger.warn("Too late! Timeframe expired after %s ms. Timeframe is %s", str(
                        elapsed_time), str(self._task_data.timeframe * 100))

                    task_report_builder = SalamiTaskReportBuilder()
                    task_report_builder.with_task_id(self._task_data.task_id)
                    task_report_builder.with_task_counter(self._task_counter.get_total_task_counter())
                    task_report_builder.with_start_time(self._transaction_info.start_time)
                    task_report_builder.with_end_time(get_current_milliseconds_time())
                    task_report_builder.with_exit_status("TIMED OUT")
                    task_report = task_report_builder.adapt_to().object()

                    self._task_counter.add_timed_out_task()
                    self._reset_task_work()

            if 'salami_message' in message:
                self._logger.info("Received ingredient update from MasterChef")
                self._start_task_work()
                salami_message = message['salami_message']
                self._process_message(salami_message)

            if self._is_done():  # all conditions are marked as checked?
                self._task_counter.add_completed_task()
                self._logger.info("**********  ALL CONDITIONS MET! *******")
                task_report_builder = SalamiTaskReportBuilder()
                task_report_builder.with_task_id(self._task_data.task_id)
                task_report_builder.with_task_counter(self._task_counter.get_total_task_counter())
                task_report_builder.with_start_time(self._transaction_info.start_time)
                task_report_builder.with_end_time(get_current_milliseconds_time())
                task_report_builder.with_exit_status("COMPLETED")
                task_report = task_report_builder.adapt_to().object()

                self._send_action_requests()
                # Hurra! Let's clear the rule checker
                self._sleep_handler()
                self._reset_task_work()
                self._logger.info("Task ended")
            if task_report is not None:
                self._notify_task_end(task_report)

    def on_clock_tick(self):
        self._logger.debug("Started Tick TASK at %s", str(get_current_milliseconds_time()))
        task_report = None
        max_timeframe = self._task_data.timeframe * 100

        if not self._sleeping:

            if self._transaction_info.start_time != 0:
                elapsed_time = get_current_milliseconds_time() - self._transaction_info.start_time
                if elapsed_time > max_timeframe:
                    self._logger.warn("Too late! Timeframe expired after %s ms. Timeframe is %s", str(
                        elapsed_time), str(self._task_data.timeframe * 100))

                    task_report_builder = SalamiTaskReportBuilder()
                    task_report_builder.with_task_id(self._task_data.task_id)
                    task_report_builder.with_task_counter(self._task_counter.get_total_task_counter())
                    task_report_builder.with_start_time(self._transaction_info.start_time)
                    task_report_builder.with_end_time(get_current_milliseconds_time())
                    task_report_builder.with_exit_status("TIMED OUT")
                    task_report = task_report_builder.adapt_to().object()

                    self._task_counter.add_timed_out_task()
                    self._reset_task_work()

            if self._crontab.active:
                if self._evaluate_crontab():
                    self._start_task_work()
                else:
                    # self.stop()
                    pass

            if self._is_done():  # all conditions are marked as checked?
                self._task_counter.add_completed_task()
                self._logger.info("**********  ALL CONDITIONS MET! *******")
                task_report_builder = SalamiTaskReportBuilder()
                task_report_builder.with_task_id(self._task_data.task_id)
                task_report_builder.with_task_counter(self._task_counter.get_total_task_counter())
                task_report_builder.with_start_time(self._transaction_info.start_time)
                task_report_builder.with_end_time(get_current_milliseconds_time())
                task_report_builder.with_exit_status("COMPLETED")
                task_report = task_report_builder.adapt_to().object()

                self._send_action_requests()
                # Hurra! Let's clear the rule checker
                self._sleep_handler()
                self._reset_task_work()
                self._logger.info("Task ended")
            if task_report is not None:
                self._notify_task_end(task_report)

    def _process_message(self, message):
        self._logger.debug("Processing message: %s", message)
        ingredient_index = message.ingredient
        if ingredient_index in self._rules:
            ingredient_value = message.command_data['value']
            self._evaluate_rules(ingredient_index, ingredient_value)

        else:
            self._logger.warn("Message refers to a non relevant ingredient. Ignored.")

    def _mark_condition(self, ingredient_index, checked):
        if checked == SalamiConstants.CONDITION_CHECKED:
            self._rule_checker_registry[ingredient_index] = True
            # status = "SUCCESS"
        else:
            self._rule_checker_registry[ingredient_index] = False
            # status = "NO SUCCESS"

        if ingredient_index == "CRONTAB":
            pass
        else:
            if self._rules[ingredient_index].is_unchained():
                for ingredient_index in self._rule_checker_registry:
                    self._rule_checker_registry[ingredient_index] = True

    def _start_task_work(self):
        if self._transaction_info.start_time == 0:
            self._logger.info("Starting new transaction")
            self._transaction_info.start_time = get_current_milliseconds_time()

    def _evaluate_crontab(self):
        if self._crontab.always:
            self._mark_condition("CRONTAB", SalamiConstants.CONDITION_CHECKED)
            return True
        else:
            if self._crontab.check():
                self._mark_condition("CRONTAB", SalamiConstants.CONDITION_CHECKED)
                return True
            else:
                self._mark_condition("CRONTAB", SalamiConstants.CONDITION_NOT_CHECKED)
                self._logger.debug("Crontab rule not verified")
                return False

    def _is_done(self):
        for ingredient_index in self._rule_checker_registry:
            if not self._rule_checker_registry[ingredient_index]:
                return False
        return True

    def _send_action_requests(self):
        # for each action_request in self.action_requests a new message is submitted to the Advertiser
        self._logger.info("Submitting task's actions to the MC Advertiser queue")
        for action_request in self._actions:
            action_request.timestamp = get_current_milliseconds_time()
            action_request.expiration = get_current_milliseconds_time() + 10000
            json_message = SalamiModelUtil.object_to_json(action_request)
            self._publisher.publish(json_message)

    def _sleep_handler(self):
        # Verify if I should sleep
        if int(self._sleep_time) != -1:
            if int(self._sleep_time) == 0:
                self._logger.info("Sleep FOREVER! Adieu!")
                self._sleeping = True
            else:
                self._logger.info("Sleep for " + str(self._sleep_time) + " seconds.")
                self._sleeping = True
                time.sleep(int(self._sleep_time))
                self._sleeping = False
                self._logger.info("yawn...I'm ready AGAIN!")

    def _notify_task_end(self, task_report):
        builder = SalamiMessageBuilder()
        builder.with_source(self._task_data.masterchef_id)
        builder.with_destination(SalamiConstants.CH_TASK_MONITOR)
        builder.with_command(SalamiConstants.CMD_NOTIFY_TASK_DONE)
        builder.with_command_data(task_report)
        task_complete = builder.adapt_to().json()
        self._publisher.publish(task_complete)

    def _reset_rule_checker_registry(self):
        self._logger.debug("Clearing ConditionCheckerRegistry")
        for ingredient_index in self._rule_checker_registry:
            self._rule_checker_registry[ingredient_index] = False
