#!/usr/bin/env bash

VIRTUAL_ENV_HOME="/opt/salamiiot/salami_venv"
PYTHON_PATH="sudo "${VIRTUAL_ENV_HOME}"/bin/python "
SALAMI_HOME="/opt/salamiiot"

START_CMD=${PYTHON_PATH}${SALAMI_HOME}"/salamiiot/slice/"

function start {
    source ${VIRTUAL_ENV_HOME}"/bin/activate"
    cd ${SALAMI_HOME}
    sudo cp -R salamiiot ${VIRTUAL_ENV_HOME}"/lib/python3.4/"
    cd salamiiot

    ${START_CMD}$1".py" &
}

function stop {
    sudo pkill -f "/salamiiot/slice/"$1
}

if [ $1 == "start" ]; then
    start $2
elif [ $1 == "stop" ]; then
    if [ $2 ]; then
        echo $2
        stop $2".py"
    else
        stop
    fi
else
    echo "Usage: slices.sh [ start - stop [slice name] ]"
fi
