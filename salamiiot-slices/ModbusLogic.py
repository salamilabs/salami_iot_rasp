'''
Created on 04/feb/2016

@author: VALERIO SINISI
'''
import datetime
import inspect
import json
import time
from inspect import currentframe, getframeinfo

import minimalmodbus
import serial

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                        SERIAL PARAMETER
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
PORT = '/dev/ttyUSB0'
BAUDRATE = 9600
BYTESIZE = 8
'''
Typically the Response time-out is from 1s to
several second at 9600 bps; and
'''
SERIAL_TIMEOUT = 1
STOPBITS = 1

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                        MODBUS PARAMETER
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''
________________________MODBUS PACKET_____________________________________________
T1 T2 T3  |  Address   |   Function  |  Data          |  CRC         |   T1 T2 T3
3,5/BAUD  |  (8 bits)  |   (8 bits)  |  (N x 8 bits)  |  (16 bits)   |   3,5/BAUD
__________________________________________________________________________________            
'''
#Il protocollo MODBUS prevede che tra un pacchetto e l'altro deve interccorrere ALMENO il tempo
#necessario alla trasmissione di 3 caratteri e mezzo.
#il tempo di un carattere e' di 11 bit (secondo specifica)

CHARACTER_TIME = 11 * (1/float(BAUDRATE))
INTERVAL = 4 * CHARACTER_TIME
INSTRUMENT_DEBUG = True
#Numero di ritrasmissioni del comando
NUMBER_OF_READ_RETRY = 5
NUMBER_OF_WRITE_RETRY = 5


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        DUCATI SMART - SLAVE PARAMETERS AND ADDRESS REGISTERS
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
DUCATI_SLAVE_ADDRESS = 2


'''
DUCATI ADDRESS
'''
#SOTTRARRE -1 nella funzione come da specifiche
Ducati_Smart_Frequency = 2                                       #Word: 2 Unit: Tenths of Hz     Format: Unsigned Long   32 bit
Ducati_Smart_Three_phase_Equivalent_Voltage = 4                  #Word: 2 Unit: V                Format: Unsigned Long
Ducati_Smart_Line_Voltage_line_1_line_2 = 6                      #Word: 2 Unit: V                Format: Unsigned Long
Ducati_Smart_Line_Voltage_line_2_line_3 = 8                      #Word: 2 Unit: V                Format: Unsigned Long
Ducati_Smart_Line_Voltage_line_3_line_1 = 10                     #Word: 2 Unit: V                Format: Unsigned Long
Ducati_Smart_Voltage_between_Phase_and_Neutral_line_1 = 12       #Word: 2 Unit: V                Format: Unsigned Long
Ducati_Smart_Voltage_between_Phase_and_Neutral_line_2 = 14       #Word: 2 Unit: V                Format: Unsigned Long
Ducati_Smart_Voltage_between_Phase_and_Neutral_line_3 = 16       #Word: 2 Unit: V                Format: Unsigned Long
Ducati_Smart_Three_phase_Equivalent_Current = 18                 #Word: 2 Unit: Hundredths of A  Format: Unsigned Long
Ducati_Smart_Current_Line_1= 20                                  #Word: 2 Unit: Hundredths of A  Format: Unsigned Long
Ducati_Smart_Current_Line_2 = 22                                 #Word: 2 Unit: Hundredths of A  Format: Unsigned Long
Ducati_Smart_Current_Line_3 = 24                                 #Word: 2 Unit: Hundredths of A  Format: Unsigned Long
Ducati_Smart_Three_phase_equivalent_power_factor = 26            #Word: 2 Unit: Hundredths       Format: bit-Signed      means a binary 2 s-complement number
Ducati_Smart_Three_phase_equivalent_active_power = 34            #Word: 2 Unit: W                Format: bit-Signed long means a binary number of 2 words (32 bits) if not, the MSB is set to 1. Forexample: 8000 0007h = -7.
Ducati_Three_Phase_Equivalent_Reactive_Power = 82                #Word: 2 Unit: VAr              Format: bit-signed long means a binary number of 2 words (32 bits) if not, the MSB is set to 1. Forexample: 8000 0007h = -7.           
Ducati_Three_Phase_Equivalent_Active_Energy = 106                #Word: 2 Unit: Wh Tents of Wh   Format: Unsigned Long   32 bit
Ducati_Three_Phase_Equivalent_Reactive_Energy = 114              #Word: 2 Unit: VArh Tents of Wh Format: Unsigned Long   32 bit


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        CURRENT TRANFORMERS - SLAVE PARAMETERS AND ADDRESS REGISTERS
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

CURRENT_TRANSFORMER_SLAVE_ADDRESS = 3

'''
Current Transformer ADDRESS
'''

""" Indirizzo LOGICO indicato sul datasheet 40014, indirizzo da passare al metodo 14, il metodo 
    deve sottrarre 1 prima di passarlo alla funzione read_register, o read_long
"""

Current_Transformer_Machine_ID = 1                              # Unsigned short      16 bit
Current_Tranformers_Firmware_Version = 2                        # Unsigned short      16 bit
Current_Transformer_Modbus_Address = 3                          # Unsigned short      16 bit
Current_Tranformers_Answer_Delay = 4                            # Unsigned short      16 bit
Current_Transformer_Baud = 5                                    # Unsigned short      16 bit
Current_Transformer_Parity = 6                                  # Unsigned short      16 bit
Start_Input_A = 7                                               # Floating            32 bits    R/W    40007 (LO)  40008 (HI)   
Stop_Input_A = 9                                                # Floating            32 bits    R/W    40009 (LO)  40009 (HI)
Start_Output_mV = 11                                            # Unsigned short      16 bits    R/W    40011
Out_stop_mV= 12                                                 # Unsigned short      16 bits    R/W    40012
Current_Tranformers_Flit_1 = 13                                 # Unsigned short      16 bit
Current_Tranformers_Filt = 14                                   # Unsigned short      16 bit
Current_Transformer_Cutoff = 29                                 # Unsigned short      16 bit
Current_Transformer_RMS_Ampere = 37                             # floating            32 bit     R      40037 (LO)  40038 (HI)
Current_Tranformers_Command = 40                                # Unsigned            16 bits    R/W    40040
Current_Transformer_Status = 48                                 # Unsigned short      16 bit
Current_Transformer_RMS_Value_of_Current_A_x_100 = 50           # signed              16 bit
Current_Tranformers_RMS_sw = 51                                 # Unsigned short      16 bit     R      40051 (HI)  40052 (LO)
Current_Tranformers_Ah = 53                                     # Unsigned short      16 bit     R/W    40053 (HI)  40054 (LO)
Current_Tranformers_A_MAX = 55                                  # Signed   short      16 bit     R/W    40055
Current_Tranformers_A_min = 56                                  # Signed   short      16 bit     R/W    40056
Current_Tranformers_Data_High = 57                              # Unsigned short      16 bit     R      40057
Current_Tranformers_Data_Medium = 58                            # Unsigned short      16 bit     R      40058
Current_Tranformers_Data_Low = 59                               # Unsigned short      16 bit     R      40059

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        PLC SIEMENS S7-300 - SLAVE PARAMETERS AND ADDRESS REGISTERS
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

PLC_SLAVE_ADDRESS = 1

'''
PLc ADDRESS
'''

""" Indirizzo LOGICO indicato sul datasheet 40014, indirizzo da passare al metodo 14, il metodo 
    deve sottrarre 1 prima di passarlo alla funzione read_register, o read_long
"""
"""
Per i registri ma modificare usare la funzione modbus 0x06 Write Single Register
"""
Plc_External_Temperature = 1                                            #int *10                
Plc_Internal_Temperature = 2                                            #int *10  
Plc_Max_Internal_Temperature_Alarm_Set_Point = 6                        #int *10                DA MODIFICARE
Plc_Freecool_1_Start_Set_Point = 7                                      #int *10                DA MODIFICARE
Plc_CDZ1_Start_Set_point = 8                                            #int *10                DA MODIFICARE
Plc_Frecool_1_Differential_Set_Point_For_Start_Stop = 9                 #int *10  
Plc_CDZ_1_Differential_Set_Point_For_Start_Stop = 10                    #int *10  
Plc_Heater_1_Start_Set_Point = 13                                       #int *10  
Plc_Freecool_2_Start_Set_Point = 27                                     #int *10                DA MODIFICARE
Plc_CDZ2_Start_Set_point = 28                                           #int *10                DA MODIFICARE
Plc_Frecool_2_Differential_Set_Point_For_Start_Stop = 29                #int *10  
Plc_CDZ_2_Differential_Set_Point_For_Start_Stop = 30                    #int *10  
Plc_Heater_2_Differential_Set_Point = 33                                #int *10  
Plc_Heater_2_Start_Set_Point = 34                                       #int *10  
Plc_Heater_1_Differential_Set_Point = 35                                #int *10  
Plc_Fan_1_Operating_Hours = 51                                          #int *10  
Plc_Compressor_1_Operating_Hours = 52                                   #int
Plc_Fan_2_Operating_Hours = 53                                          #int
Plc_Compressor_2_Operating_Hours = 54                                   #int
Plc_Hours_And_Minute_To_Exchange_Set_Point = 81                         #int *10  
Plc_Emergency_Freecool_Temperature_Set_Point = 87                       #int *10  
Plc_Max_Speed_Temperature_Set_Point = 95                                #int *10  
Plc_Min_Differential_External_Temperature_Set_Point = 141               #int *10  
Plc_Max_External_Temperature_Threshould_Set_Point = 144                 #int *10                DA MODIFICARE
Plc_Min_External_Temperature_Threshold_set_Point = 201                  #int *10                DA MODIFICARE
Plc_Alarm_Word_1 = 152                                                  #WORD        16 bit
Plc_Alarm_Word_2 = 153                                                  #WORD        16 bit


"""
************************
ALARM WORD 1 DESCRIPTION
************************
|Bit 1          |Bit 2         |Bit 3         |Bit 4          |Bit 5         |Bit 6         |Bit 7            |Bit 8          | 
|SMOKE DETECTOR |FAN 1 SWITCH  |FAN 2 SWITCH  |COMPRESSOR 1   |COMPRESSOR 2  |POWER         |CONDITIONER 2    |CONDITIONER 1  |
|ALARM          |ALARM         |ALARM         |SWITCH ALARM   |SWITCH ALARM  |FAILURE       |FAILURE TO START |TEMP ALARM     |

|Bit 9          |Bit 10        |Bit 11        |Bit 12         |Bit 13         |Bit 14        |Bit 15        |Bit 16           |
|CONDITIONER 2  |CDZ 1         |CDZ 2         |FAULT EXTERNAL |FAULT INTERNAL |INVERTER IS   |MAX INTERNAL  |CONDITIONER 1    |
|TEMP ALARM     |DIRTY FILTER  |DIRTY FILTER  |TEMP SENSOR    |TWMP SENSOR    |RUNNING       |TEMPERATURE   |FAILURE TO START |


************************
ALARM WORD 2 DESCRIPTION
************************
|Bit 1     |Bit 2     |Bit 3     |Bit 4     |Bit 5     |Bit 6    |Bit 7    |Bit 8     | 
|NOT USED  |NOT USED  |NOT USED  |NOT USED  |NOT USED  |INVERTER |NOT USED |NOT USED  | 
|          |          |          |          |          |ALARM    |         |          |

|Bit 9     |Bit 10    |Bit 11    |Bit 12    |Bit 13    |Bit 14    |Bit 15   |Bit 16   |
|NOT USED  |NOT USED  |NOT USED  |NOT USED  |NOT USED  |NOT USED  |NOT USED |NOT USED |

"""



################################################
#LOG HELPER FUNCTION
################################################
def PrintError(method, err, line):
    print("ERROR (Line: " +str(line) +')'+ ' ' + '[' + str(method) + ']'+ ': ' + str(err) + ' - ' + str(type(err)))

def PrintModbusError(method, ValueError, TypeError, IOError, line):
    
    print("ERROR (Line: " +str(line) +')'+ ' ' + '[' + str(method) + ']'+ ': ' + str(ValueError))
    print("ERROR (Line: " +str(line) +')'+ ' ' + '[' + str(method) + ']'+ ': ' + str(TypeError))
    print("ERROR (Line: " +str(line) +')'+ ' ' + '[' + str(method) + ']'+ ': ' + str(IOError))
'''    
def method(self):
    try:
        
        method
        
    except Exception, err:
        PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
'''
#################################################

def j_default(o):
    return o.__dict__

       
class Modbus:

    def __init__(self, slave_address, device_name, debug_mode):
        try:
            '''
            SERIAL PARAMETERS
            '''
            self.instrument_name = '[' + str(device_name) + ']'
            self.instrument_debug = debug_mode
            
            self.instrument = minimalmodbus.Instrument(PORT, slave_address) # port name, slave address (in decimal)
            self.instrument.serial.baudrate = BAUDRATE
            self.instrument.serial.bytesize = BYTESIZE
            self.instrument.serial.parity   = serial.PARITY_NONE
            self.instrument.serial.stopbits = STOPBITS
            self.instrument.serial.timeout  = SERIAL_TIMEOUT  
            
            
            '''
            INSTRUMENT PARAMETERS
            '''
            
            self.instrument.address = slave_address
            self.instrument.mode = minimalmodbus.MODE_RTU   # rtu or ascii mode   
            self.instrument.debug = debug_mode
             
            

        except Exception as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
  
    def PrintDeviceInfo(self):
        print("_______________ SERIAL INFO: ______________")
        print("PORT:\t\t\t\t" + str(self.instrument.serial.port))
        print("BAUD:\t\t\t\t" + str(self.instrument.serial.baudrate))
        print("BYTESIZE:\t\t\t" + str(self.instrument.serial.bytesize))
        print("PARITY:\t\t\t\t" + str(self.instrument.serial.parity))
        print("STOPBITS:\t\t\t" + str(self.instrument.serial.stopbits))
        print("TIMEOUT:\t\t\t" + str(self.instrument.serial.timeout)) # seconds
        print("INTERCHAR TIMEOUT:\t\t" + str(self.instrument.serial.interCharTimeout))
        print("CLOSE PORT AFETER EACH CALL:\t" + str(self.instrument.close_port_after_each_call))
        print("INTERVAL BETWEEN MESSAGES:\t:" + str(INTERVAL))
        print('\n')
        print("_______________ " + str(self.instrument_name) + " DEVICE INFO: _______________")
        print("ADDRESS:\t\t\t" + str(self.instrument.address))
        print("MODE:\t\t\t\t" + str(self.instrument.mode))
        print("DEBUG MODE:\t\t\t" + str(self.instrument.debug))   
        print("\n************** \nSerial Opened \n**************\n")
    
    def Print_Value(self, value_name, value, unit):
        if value is not None:        
            try:
                print(str(self.instrument_name) + " - " + str(value_name) + ": " + str(value) + ' ' + str(unit))
            except Exception as err:
                PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
        else:
            #print('Dato ' + str(name) + ' non valido' 
            print(str(self.instrument_name) + " - " + str(value_name) + ": " + str('Not Valid Data'))

    """
    ***************************************
          MODBUS READ REGISTER METHOD
    ***************************************
    """
                            
    def Single_Read_Long_CMD(self, register_address):
        #Singolo Comando di lettura

        try:
            #time.sleep(INTERVAL)    
            value = self.instrument.read_long(register_address-1)  # Registernumber, number of decimals
            time.sleep(INTERVAL)
            return value
             
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno)
            print('Command sent: ' + str(register_address));
    
    def Read_Long_CMD(self, register_address, numeber_of_attempts, number_of_decimals):
        #Comando con ritrasmissioni in caso di errore

        value = None
        for i in range(0,numeber_of_attempts):
            if self.instrument_debug == True:
                print("Attempt No: " + str(i+1))
            value = self.Single_Read_Long_CMD(register_address)  
            if value is not None:
                break
            
        if value is not None:
            if number_of_decimals == 0:
                return value
            elif number_of_decimals == 1:
                return float(value)/10
            elif number_of_decimals == 2:
                return float(value)/100
        else:
            return value

    def Single_Read_CMD(self, register_address, number_of_decimals):
        #Singolo Comando di lettura

        try:
            #time.sleep(INTERVAL)    
            value = self.instrument.read_register(register_address-1, number_of_decimals)  # Registernumber, number of decimals
            time.sleep(INTERVAL)
            return value
             
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno)
            print('Command sent: ' + str(register_address));
    
    def Read_CMD(self, register_address, numeber_of_attempts, number_of_decimals):
        #Comando con ritrasmissioni in caso di errore

        value = None
        for i in range(0,numeber_of_attempts):
            if self.instrument_debug == True:
                print("Attempt No: " + str(i+1))
            value = self.Single_Read_CMD(register_address, number_of_decimals)

            if value is not None:
                break
        return value
       
    """
    ***************************************
          MODBUS WRITE REGISTER METHOD
    ***************************************
    """
           
    def Single_Write_CMD(self, register_address, value, number_of_decimals):
        #Singolo Comando di lettura
            
        try:
            time.sleep(INTERVAL) 
            #self.instrument.write_register(register_address-1, value, number_of_decimals)   
            self.instrument.write_register(register_address-1, value, number_of_decimals, functioncode=6)  # Registernumber, number of decimals
            time.sleep(INTERVAL)
             
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno)
            print('Command sent: ' + str(register_address));
    
    def Write_CMD(self, register_address, value, numeber_of_attempts, number_of_decimals):
        #Comando con ritrasmissioni in caso di errore
        
        for i in range(0,numeber_of_attempts):
            if self.instrument_debug == True:
                print("Attempt No: " + str(i+1))
            self.Single_Write_CMD(register_address, value, number_of_decimals)

            if value is not None:
                break
        return value            

    """
    *************************
       DUCATI GET METHOD
    *************************
    """
    def Get_Ducati_Three_Phase_Equivalent_Voltage(self):
        try:
            value = self.Read_Long_CMD(Ducati_Smart_Three_phase_Equivalent_Voltage, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno)
            return None
        return value

    def Get_Ducati_Frequency(self):
        try:
            value = self.Read_Long_CMD(Ducati_Smart_Frequency, NUMBER_OF_READ_RETRY, 1) 
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno)
            return None
        return value
    
    def Get_Ducati_Power_Factor(self):
        try:
            value = self.Read_Long_CMD(Ducati_Smart_Three_phase_equivalent_power_factor, NUMBER_OF_READ_RETRY, 2)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno)
            return None
        return value    

    def Get_Ducati_Active_Power(self):
        try:
            value = self.Read_Long_CMD(Ducati_Smart_Three_phase_equivalent_active_power, NUMBER_OF_READ_RETRY, 0);
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno)
            return None
        
        if value & 0x80000000 == 1:
            pass
            #negative number
            return -(value & 0x7FFFFFFF)
        else:
            #positive number
            return value
        
    def Get_Ducati_Active_Energy(self):
        try:
            value = self.Read_Long_CMD(Ducati_Three_Phase_Equivalent_Active_Energy, NUMBER_OF_READ_RETRY, 1);
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno)
            return None

        return value

    def Get_Ducati_Reactive_Power(self):
        try:
            value = self.Read_Long_CMD(Ducati_Three_Phase_Equivalent_Reactive_Power, NUMBER_OF_READ_RETRY, 0);
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno)
            return None        
        return value
                    
    def Get_Ducati_Reactive_Energy(self):
        try:
            value = self.Read_Long_CMD(Ducati_Three_Phase_Equivalent_Reactive_Energy, NUMBER_OF_READ_RETRY, 1);
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno)
            return None

        return value
                                                            
    def Ducati_Test(self):
        try:
            print('***************************************************************')
            print('                  - [Ducati Multimeter AC] -                   ')
            print('***************************************************************')
            print('\n' )           

            _Three_phase_Equivalent_Voltage         = self.Get_Ducati_Three_Phase_Equivalent_Voltage()
            _Three_phase_equivalent_power_factor    = self.Get_Ducati_Power_Factor()
            _Frequency                              = self.Get_Ducati_Frequency()
            _Three_phase_equivalent_active_power    = self.Get_Ducati_Active_Power()
            _Three_phase_aquivalent_active_Energy   = self.Get_Ducati_Active_Energy()
            _Three_phase_equivalent_reactive_power  = self.Get_Ducati_Reactive_Power()
            _Three_phase_equivalent_reactive_energy = self.Get_Ducati_Reactive_Energy()
            
            self.Print_Value('Three_phase_Equivalent_Voltage\t\t', _Three_phase_Equivalent_Voltage, '[V]')
            self.Print_Value('Three_phase_equivalent_power_factor\t', _Three_phase_equivalent_power_factor, ' ')
            self.Print_Value('Frequency\t\t\t\t', _Frequency, '[Hz]')
            self.Print_Value('Three_phase_equivalent_Active_power\t', _Three_phase_equivalent_active_power, '[W]')  
            self.Print_Value('Three_Phase_Equivalent_Active_Energy\t', _Three_phase_aquivalent_active_Energy, '[Wh]')
            self.Print_Value('Three_Phase_Equivalent_Reactive_Power\t', _Three_phase_equivalent_reactive_power, '[VAr]')
            self.Print_Value('Three_Phase_Equivalent_Reactive_Energy\t', _Three_phase_equivalent_reactive_energy, '[VArh]')
            
            print('\n')
            
            """
            _Three_phase_Equivalent_Voltage         = self.Read_Long_CMD(Ducati_Smart_Three_phase_Equivalent_Voltage, NUMBER_OF_READ_RETRY, 0)
            _Three_phase_equivalent_active_power    = self.Read_Long_CMD(Ducati_Smart_Three_phase_equivalent_active_power, NUMBER_OF_READ_RETRY, 0);
            _Three_phase_equivalent_power_factor    = self.Read_Long_CMD(Ducati_Smart_Three_phase_equivalent_power_factor, NUMBER_OF_READ_RETRY, 2)
            _Frequency                              = self.Read_Long_CMD(Ducati_Smart_Frequency, NUMBER_OF_READ_RETRY, 1) 
    
            _Three_phase_Equivalent_Current         = self.Read_Long_CMD(Ducati_Smart_Three_phase_Equivalent_Current, NUMBER_OF_READ_RETRY, 2)
            _Line_Voltage_line_1_line_2             = self.Read_Long_CMD(Ducati_Smart_Line_Voltage_line_1_line_2, NUMBER_OF_READ_RETRY, 0)
            _Line_Voltage_line_2_line_3             = self.Read_Long_CMD(Ducati_Smart_Line_Voltage_line_2_line_3, NUMBER_OF_READ_RETRY, 0)
            _Line_Voltage_line_3_line_1             = self.Read_Long_CMD(Ducati_Smart_Line_Voltage_line_3_line_1, NUMBER_OF_READ_RETRY, 0)
            _Current_Line_1                         = self.Read_Long_CMD(Ducati_Smart_Current_Line_1, NUMBER_OF_READ_RETRY, 2)
            _Current_Line_2                         = self.Read_Long_CMD(Ducati_Smart_Current_Line_2, NUMBER_OF_READ_RETRY, 2)
            _Current_Line_3                         = self.Read_Long_CMD(Ducati_Smart_Current_Line_3, NUMBER_OF_READ_RETRY, 2)
            
            self.Print_Value('Three_phase_Equivalent_Voltage', _Three_phase_Equivalent_Voltage, '[V]')
            self.Print_Value('Three_phase_equivalent_active_power', _Three_phase_equivalent_active_power, '[W]')     
            self.Print_Value('Three_phase_equivalent_power_factor', _Three_phase_equivalent_power_factor, ' ')            
            self.Print_Value('Frequency', _Frequency, '[Hz]')

            self.Print_Value('Three_phase_Equivalent_Current', _Three_phase_Equivalent_Current, '[A]')
            
            
            self.Print_Value('Line_Voltage_line_1_line_2', _Line_Voltage_line_1_line_2, '[Hz]')
            self.Print_Value('Line_Voltage_line_2_line_3', _Line_Voltage_line_2_line_3, '[Hz]')
            self.Print_Value('Line_Voltage_line_3_line_1', _Line_Voltage_line_3_line_1, '[Hz]')
            self.Print_Value('Current_Line_1', _Current_Line_1, '[A]')
            self.Print_Value('Current_Line_2', _Current_Line_2, '[A]')
            self.Print_Value('Current_Line_3', _Current_Line_3, '[A]')
            """
            print('\n')
                
        except Exception as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);


    """
    ***************************************
        CURRENT TRANSFORMER GET METHOD
    ***************************************
    """
    def Get_Current_Transformers_RMS_Ampere(self):
        try:
            value = self.Read_Long_CMD(Current_Transformer_RMS_Ampere, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno)
            return None
        return value    

    def Current_Transformer_Test(self):
        try:
            
            print('***************************************************************')
            print('                 - [Current Tranformer DC] -                   ')
            print('***************************************************************' )           
            print('\n')
            
            """
                L'indirizzo del registro modbus va passato come da datasheet (senza il 4 iniziale). Ci
                Pensa la funzione a sottrarre -1.
                unsigned shot 16 bit read_CMD
            """

            
            Machine_ID              = self.Read_CMD(Current_Transformer_Machine_ID, NUMBER_OF_READ_RETRY, 0)
            Modbus_Address          = self.Read_CMD(Current_Transformer_Modbus_Address, NUMBER_OF_READ_RETRY, 0)
            Answer_Delay            = self.Read_CMD(Current_Tranformers_Answer_Delay, NUMBER_OF_READ_RETRY, 0)
            baud                    = self.Read_CMD(Current_Transformer_Baud, NUMBER_OF_READ_RETRY, 0)
            #RMS_ampere              = self.Read_CMD(Current_Transformer_RMS_Ampere, NUMBER_OF_READ_RETRY, 0)
            RMS_ampere_X100         = self.Read_CMD(Current_Transformer_RMS_Value_of_Current_A_x_100, NUMBER_OF_READ_RETRY, 2)
            
            '''
            RMS_Ampere              = self.Read_CMD(Current_Transformer_RMS_Ampere, NUMBER_OF_READ_RETRY, 0)
            RMS_Ampere_A_x_100      = self.Read_CMD(Current_Transformer_RMS_Value_of_Current_A_x_100, NUMBER_OF_READ_RETRY, 0)
            Status                  = self.Read_CMD(Current_Transformer_Status, NUMBER_OF_READ_RETRY, 0)
            Current_Cutoff          = self.Read_CMD(Current_Transformer_Cutoff, NUMBER_OF_READ_RETRY, 0)
            Flit1                   = self.Read_CMD(Current_Tranformers_Flit_1, NUMBER_OF_READ_RETRY, 0)
            Flit                    = self.Read_CMD(Current_Tranformers_Filt, NUMBER_OF_READ_RETRY, 0)
            '''   
            
            self.Print_Value('Machine_ID', Machine_ID, ' ')
            self.Print_Value('BAUD', baud, ' ')
            self.Print_Value('Modbus Address', Modbus_Address, ' ')
            self.Print_Value('Ansewer Delay', Answer_Delay, ' ')
            #self.Print_Value('RMS Ampere', RMS_ampere, '[A]')
            self.Print_Value('RMS Ampere X100', RMS_ampere_X100, '[A]')
            
            print('\n')
            '''
            self.Print_Value('RMS Ampere', RMS_Ampere, ' ')
            self.Print_Value('RMS Ampere x100', RMS_Ampere_A_x_100, ' ')
            self.Print_Value('Status', Status, ' ')
            self.Print_Value('Current_Cutoff', Current_Cutoff, 'mA')
            self.Print_Value('No_Of_Samples', Flit1*100, 'mS')
            self.Print_Value('Second_Level Filter', Flit, ' ')
            '''
            
            #RESET                   = self.Read_CMD(Current_Tranformers_Command, NUMBER_OF_READ_RETRY, 0)
            
            #self.Print_Value('RESET ', RESET , ' ')
            
            """
            print("_________________-PORVA WRITE-____________________"
            self.Single_Write_CMD(Current_Tranformers_Flit_1, 1, 0)
            New_Current_Tranformers_Flit_1 = self.Read_CMD(Current_Tranformers_Flit_1, NUMBER_OF_READ_RETRY, 0)
            self.Print_Value('No_Of_Samples', New_Current_Tranformers_Flit_1, 'mS')
            """
            
        except Exception as  err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);


    """
    ***************************************
                PLC GET METHOD
    ***************************************
    """
    
    """
************************
ALARM WORD 1 DESCRIPTION
************************
|Bit 1          |Bit 2         |Bit 3         |Bit 4          |Bit 5         |Bit 6         |Bit 7            |Bit 8          | 
|SMOKE DETECTOR |FAN 1 SWITCH  |FAN 2 SWITCH  |COMPRESSOR 1   |COMPRESSOR 2  |POWER         |CONDITIONER 2    |CONDITIONER 1  |
|ALARM          |ALARM         |ALARM         |SWITCH ALARM   |SWITCH ALARM  |FAILURE       |FAILURE TO START |TEMP ALARM     |

|Bit 9          |Bit 10        |Bit 11        |Bit 12         |Bit 13         |Bit 14        |Bit 15        |Bit 16           |
|CONDITIONER 2  |CDZ 1         |CDZ 2         |FAULT EXTERNAL |FAULT INTERNAL |INVERTER IS   |MAX INTERNAL  |CONDITIONER 1    |
|TEMP ALARM     |DIRTY FILTER  |DIRTY FILTER  |TEMP SENSOR    |TWMP SENSOR    |RUNNING       |TEMPERATURE   |FAILURE TO START |


************************
ALARM WORD 2 DESCRIPTION
************************
|Bit 1     |Bit 2     |Bit 3     |Bit 4     |Bit 5     |Bit 6    |Bit 7    |Bit 8     | 
|NOT USED  |NOT USED  |NOT USED  |NOT USED  |NOT USED  |INVERTER |NOT USED |NOT USED  | 
|          |          |          |          |          |ALARM    |         |          |

|Bit 9     |Bit 10    |Bit 11    |Bit 12    |Bit 13    |Bit 14    |Bit 15   |Bit 16   |
|NOT USED  |NOT USED  |NOT USED  |NOT USED  |NOT USED  |NOT USED  |NOT USED |NOT USED |

"""
    def Get_Smoke_Detector_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000001

    def Get_Fan_1_Switch_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000002

    def Get_Fan_2_Switch_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000004

    def Get_Compressor_1_Switch_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000008
    
    def Get_Compressor_2_Switch_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000010

    def Get_Power_Failure_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000020

    def Get_Conditioner_2_Failure_To_Start_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000040

    def Get_Conditioner_1_Temp_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000080  

    def Get_Conditioner_2_Temp_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000100
    
    def Get_CDZ_1_Dirty_Filter_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000200      

    def Get_CDZ_2_Dirty_Filter_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000400   

    def Get_Fault_External_Temperature_Sensor_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000800 

    def Get_Fault_Internal_Temperature_Sensor_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00001000 

    def Get_Inverter_Is_Running_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00002000 

    def Get_Max_Internal_Temperature_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00004000 

    def Get_Conditioner_1_Failure_To_Start_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00008000 

    def Get_Inverter_Alarm(self):
        try:
            word = self.Read_CMD(Plc_Alarm_Word_2, NUMBER_OF_READ_RETRY, 0)
        except (ValueError, TypeError, IOError) as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);
            return None
        return word & 0x00000020 
                            
    def Plc_Test(self):
        try:
            print('***************************************************************')
            print('                           - [PLC] -                           ')
            print('***************************************************************')
            print('\n')
            
            
            _Plc_External_Temperature                               = self.Read_CMD(Plc_External_Temperature, NUMBER_OF_READ_RETRY, 1)
            _Plc_Internal_Temperature                               = self.Read_CMD(Plc_Internal_Temperature, NUMBER_OF_READ_RETRY, 1)
            _Plc_Max_Internal_Temperature_Alarm_Set_Point           = self.Read_CMD(Plc_Max_Internal_Temperature_Alarm_Set_Point, NUMBER_OF_READ_RETRY, 1)
            _Plc_Freecool_1_Start_Set_Point                         = self.Read_CMD(Plc_Freecool_1_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)            
            _Plc_CDZ1_Start_Set_point                               = self.Read_CMD(Plc_CDZ1_Start_Set_point, NUMBER_OF_READ_RETRY, 1)            
            _Plc_Frecool_1_Differential_Set_Point_For_Start_Stop    = self.Read_CMD(Plc_Frecool_1_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
            _Plc_CDZ_1_Differential_Set_Point_For_Start_Stop        = self.Read_CMD(Plc_CDZ_1_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
 
            _Plc_Heater_1_Start_Set_Point                           = self.Read_CMD(Plc_Heater_1_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)
            _Plc_Freecool_2_Start_Set_Point                         = self.Read_CMD(Plc_Freecool_2_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)
            _Plc_CDZ2_Start_Set_point                               = self.Read_CMD(Plc_CDZ2_Start_Set_point, NUMBER_OF_READ_RETRY, 1)
            _Plc_Frecool_2_Differential_Set_Point_For_Start_Stop    = self.Read_CMD(Plc_Frecool_2_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
            _Plc_CDZ_2_Differential_Set_Point_For_Start_Stop        = self.Read_CMD(Plc_CDZ_2_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
            _Plc_Heater_2_Differential_Set_Point                    = self.Read_CMD(Plc_Heater_2_Differential_Set_Point, NUMBER_OF_READ_RETRY, 1)
            _Plc_Heater_2_Start_Set_Point                           = self.Read_CMD(Plc_Heater_2_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)
            _Plc_Heater_1_Differential_Set_Point                    = self.Read_CMD(Plc_Heater_1_Differential_Set_Point , NUMBER_OF_READ_RETRY, 1)
            _Plc_Fan_1_Operating_Hours                              = self.Read_CMD(Plc_Fan_1_Operating_Hours, NUMBER_OF_READ_RETRY, 0)

            _Plc_Compressor_1_Operating_Hours                       = self.Read_CMD(Plc_Compressor_1_Operating_Hours, NUMBER_OF_READ_RETRY, 0)
            _Plc_Fan_2_Operating_Hours                              = self.Read_CMD(Plc_Fan_2_Operating_Hours, NUMBER_OF_READ_RETRY, 0)
            _Plc_Compressor_2_Operating_Hours                       = self.Read_CMD(Plc_Compressor_2_Operating_Hours, NUMBER_OF_READ_RETRY, 0)
            _Plc_Hours_And_Minute_To_Exchange_Set_Point             = self.Read_CMD(Plc_Hours_And_Minute_To_Exchange_Set_Point, NUMBER_OF_READ_RETRY, 1)
            _Plc_Emergency_Freecool_Temperature_Set_Point           = self.Read_CMD(Plc_Emergency_Freecool_Temperature_Set_Point, NUMBER_OF_READ_RETRY, 1)
            _Plc_Max_Speed_Temperature_Set_Point                    = self.Read_CMD(Plc_Max_Speed_Temperature_Set_Point, NUMBER_OF_READ_RETRY, 1)
            _Plc_Min_Differential_External_Temperature_Set_Point    = self.Read_CMD(Plc_Min_Differential_External_Temperature_Set_Point, NUMBER_OF_READ_RETRY, 1)
            _Plc_Max_External_Temperature_Threshould_Set_Point      = self.Read_CMD(Plc_Max_External_Temperature_Threshould_Set_Point, NUMBER_OF_READ_RETRY, 1)
            _Plc_Min_External_Temperature_Threshold_set_Point       = self.Read_CMD(Plc_Min_External_Temperature_Threshold_set_Point, NUMBER_OF_READ_RETRY, 1)
            
            
            _Plc_Alarm_Word_1                                       = self.Read_CMD(Plc_Alarm_Word_1, NUMBER_OF_READ_RETRY, 0)
            _Plc_Alarm_Word_2                                       = self.Read_CMD(Plc_Alarm_Word_2, NUMBER_OF_READ_RETRY, 0)
            
            
            
            self.Print_Value('PLC_Internal_Temperature\t\t\t\t', _Plc_Internal_Temperature, ' ')
            self.Print_Value('PLC_External_Temperature\t\t\t\t', _Plc_External_Temperature, ' ')
            self.Print_Value('PLC_Max_Internal_Temperature_Alarm_Set_Point\t\t', _Plc_Max_Internal_Temperature_Alarm_Set_Point, ' ')
            self.Print_Value('Plc_Freecool_1_Start_Set_Point\t\t\t\t', _Plc_Freecool_1_Start_Set_Point, ' ')
            self.Print_Value('Plc_CDZ1_Start_Set_point\t\t\t\t', _Plc_CDZ1_Start_Set_point, ' ')
            self.Print_Value('Plc_Frecool_1_Differential_Set_Point_For_Start_Stop\t', _Plc_Frecool_1_Differential_Set_Point_For_Start_Stop, ' ')
            self.Print_Value('Plc_CDZ_1_Differential_Set_Point_For_Start_Stop\t\t',_Plc_CDZ_1_Differential_Set_Point_For_Start_Stop , ' ')
            self.Print_Value('Plc_Heater_1_Start_Set_Point\t\t\t\t' ,_Plc_Heater_1_Start_Set_Point ,' ' )
            self.Print_Value('Plc_Freecool_2_Start_Set_Point\t\t\t\t' ,_Plc_Freecool_2_Start_Set_Point ,' ' )
            self.Print_Value('Plc_CDZ2_Start_Set_point\t\t\t\t',_Plc_CDZ2_Start_Set_point ,' ' )
            self.Print_Value('Plc_Frecool_2_Differential_Set_Point_For_Start_Stop\t' ,_Plc_Frecool_2_Differential_Set_Point_For_Start_Stop ,' ' )
            self.Print_Value('Plc_Heater_2_Differential_Set_Point\t\t\t',_Plc_Heater_2_Differential_Set_Point ,' ' )
            self.Print_Value('Plc_Heater_2_Start_Set_Point\t\t\t\t' ,_Plc_Heater_2_Start_Set_Point ,' ' )
            self.Print_Value('Plc_Heater_1_Differential_Set_Point\t\t\t' ,_Plc_Heater_1_Differential_Set_Point ,' ' )
            self.Print_Value('Plc_Fan_1_Operating_Hours\t\t\t\t',_Plc_Fan_1_Operating_Hours ,' ' )
            self.Print_Value('Plc_Compressor_1_Operating_Hours\t\t\t',_Plc_Compressor_1_Operating_Hours ,' ' )
            self.Print_Value('Plc_Fan_2_Operating_Hours\t\t\t\t',_Plc_Fan_2_Operating_Hours ,' ' )
            self.Print_Value('Plc_Compressor_2_Operating_Hours\t\t\t',_Plc_Compressor_2_Operating_Hours ,' ' )
            self.Print_Value('Plc_Hours_And_Minute_To_Exchange_Set_Point\t\t',_Plc_Hours_And_Minute_To_Exchange_Set_Point ,' ' )
            self.Print_Value('Plc_Emergency_Freecool_Temperature_Set_Point\t\t',_Plc_Emergency_Freecool_Temperature_Set_Point ,' ' )
            self.Print_Value('Plc_Max_Speed_Temperature_Set_Point\t\t\t',_Plc_Max_Speed_Temperature_Set_Point ,' ' )
            self.Print_Value('Plc_Min_Differential_External_Temperature_Set_Point\t',_Plc_Min_Differential_External_Temperature_Set_Point ,' ' )
            self.Print_Value('Plc_Max_External_Temperature_Threshould_Set_Point\t',_Plc_Max_External_Temperature_Threshould_Set_Point ,' ' )
            self.Print_Value('Plc_Min_External_Temperature_Threshold_set_Point\t',_Plc_Min_External_Temperature_Threshold_set_Point ,' ' )
            
            
            self.Print_Value('Plc_Alarm_Word_1\t\t\t\t\t',_Plc_Alarm_Word_1  ,' ' )
            self.Print_Value('Plc_Alarm_Word_2\t\t\t\t\t',_Plc_Alarm_Word_2 ,' ' )  
            print('\n') 
            
            
            _Smoke_Detector_Alarm = self.Get_Smoke_Detector_Alarm()
            _Fan_1_Switch_Alarm = self.Get_Fan_1_Switch_Alarm()
            _Fan_2_Switch_Alarm = self.Get_Fan_2_Switch_Alarm()
            _Compressor_1_Switch_Alarm = self.Get_Compressor_1_Switch_Alarm()
            _Compressor_2_Switch_Alarm = self.Get_Compressor_2_Switch_Alarm()
            _Power_Failure_Alarm = self.Get_Power_Failure_Alarm()
            _Conditioner_2_Failure_To_Start_Alarm = self.Get_Conditioner_2_Failure_To_Start_Alarm()
            _Conditioner_1_Temp_Alarm = self.Get_Conditioner_1_Temp_Alarm()
            _Conditioner_2_Temp_Alarm = self.Get_Conditioner_2_Temp_Alarm()
            _CDZ_1_Dirty_Filter_Alarm = self.Get_CDZ_1_Dirty_Filter_Alarm()    
            _CDZ_2_Dirty_Filter_Alarm = self.Get_CDZ_2_Dirty_Filter_Alarm()
            _Fault_External_Temperature_Sensor_Alarm = self.Get_Fault_External_Temperature_Sensor_Alarm()
            _Fault_Internal_Temperature_Sensor_Alarm = self.Get_Fault_Internal_Temperature_Sensor_Alarm()
            _Inverter_Is_Running_Alarm = self.Get_Inverter_Is_Running_Alarm()
            _Max_Internal_Temperature_Alarm = self.Get_Max_Internal_Temperature_Alarm()
            _Conditioner_1_Failure_To_Start_Alarm = self.Get_Conditioner_1_Failure_To_Start_Alarm()
            _Inverter_Alarm =  self.Get_Inverter_Alarm()   
            
            print('\n________________________ - PLC ALARMS - _______________________\n') 
            
            self.Print_Value('Get_Smoke_Detector_Alarm\t\t', _Smoke_Detector_Alarm, ' ')
            self.Print_Value('Fan_1_Switch_Alarm\t\t\t',_Fan_1_Switch_Alarm , ' ')
            self.Print_Value('Fan_2_Switch_Alarm\t\t\t',_Fan_2_Switch_Alarm , ' ')
            self.Print_Value('Compressor_1_Switch_Alarm\t\t',_Compressor_1_Switch_Alarm , ' ')
            self.Print_Value('Compressor_2_Switch_Alarm\t\t',_Compressor_2_Switch_Alarm , ' ')
            self.Print_Value('Power_Failure_Alarm\t\t\t',_Power_Failure_Alarm , ' ')
            self.Print_Value('Conditioner_2_Failure_To_Start_Alarm\t',_Conditioner_2_Failure_To_Start_Alarm , ' ')
            self.Print_Value('Conditioner_1_Temp_Alarm\t\t',_Conditioner_1_Temp_Alarm , ' ')
            self.Print_Value('Conditioner_2_Temp_Alarm\t\t',_Conditioner_2_Temp_Alarm , ' ')
            self.Print_Value('CDZ_1_Dirty_Filter_Alarm\t\t',_CDZ_1_Dirty_Filter_Alarm , ' ')
            self.Print_Value('CDZ_2_Dirty_Filter_Alarm\t\t',_CDZ_2_Dirty_Filter_Alarm , ' ')
            self.Print_Value('Fault_External_Temperature_Sensor_Alarm\t',_Fault_External_Temperature_Sensor_Alarm , ' ')
            self.Print_Value('Fault_Internal_Temperature_Sensor_Alarm\t',_Fault_Internal_Temperature_Sensor_Alarm , ' ')
            self.Print_Value('Inverter_Is_Running_Alarm\t\t',_Inverter_Is_Running_Alarm , ' ')
            self.Print_Value('Max_Internal_Temperature_Alarm\t\t',_Max_Internal_Temperature_Alarm , ' ')
            self.Print_Value('Conditioner_1_Failure_To_Start_Alarm\t',_Conditioner_1_Failure_To_Start_Alarm , ' ')
            self.Print_Value('Inverter_Alarm\t\t\t\t',_Inverter_Alarm , ' ')

            
            print('\n__________________ - PLC WRITE OPRATIONS - ____________________\n')
            
            NEW_FREE_COOL_VALUE = 35.0
            NEW_CDZ_2_START_SET_POINT = 25.0 #hold 32.0
            
            self.Write_CMD(Plc_Freecool_1_Start_Set_Point, NEW_FREE_COOL_VALUE, NUMBER_OF_WRITE_RETRY, 1)
            _NEW_Plc_Freecool_1_Start_Set_Point = self.Read_CMD(Plc_Freecool_1_Start_Set_Point, NUMBER_OF_READ_RETRY, 1) 
            self.Print_Value('Plc_Freecool_1_Start_Set_Point', _NEW_Plc_Freecool_1_Start_Set_Point, ' ')
            
            self.Write_CMD(Plc_Freecool_2_Start_Set_Point, NEW_FREE_COOL_VALUE, NUMBER_OF_WRITE_RETRY, 1)
            _NEW_Plc_Freecool_2_Start_Set_Point = self.Read_CMD(Plc_Freecool_2_Start_Set_Point, NUMBER_OF_READ_RETRY, 1) 
            self.Print_Value('Plc_Freecool_2_Start_Set_Point', _NEW_Plc_Freecool_1_Start_Set_Point, ' ')

            self.Write_CMD(Plc_CDZ2_Start_Set_point, NEW_CDZ_2_START_SET_POINT, NUMBER_OF_WRITE_RETRY, 1)
            _NEW_Plc_CDZ2_Start_Set_point = self.Read_CMD(Plc_Freecool_2_Start_Set_Point, NUMBER_OF_READ_RETRY, 1) 
            self.Print_Value('NEW_Plc_CDZ2_Start_Set_point', _NEW_Plc_CDZ2_Start_Set_point, ' ')                       
            
            print('\n__________________ - END WRITE OPRATIONS - ____________________\n')
            print('\n')
            
        except Exception as err:
            PrintError(inspect.currentframe().f_code.co_name, err, getframeinfo(currentframe()).lineno);


class Measure_Ducati:
    def __init__(self):
        self.BUS_Address    = DUCATI_SLAVE_ADDRESS      
        self.AC_Voltage = None
        self.AC_Active_Power = None
        self.AC_Reactive_Power = None
        self.Power_Factor = None
        self.Frequency = None

class Measure_Plc():
    def __init__(self):
        
        self.BUS_Address    = PLC_SLAVE_ADDRESS
        self.Plc_External_Temperature                               = None
        self.Plc_Internal_Temperature                               = None
        self.Plc_Max_Internal_Temperature_Alarm_Set_Point           = None
        self.Plc_Freecool_1_Start_Set_Point                         = None           
        self.Plc_CDZ1_Start_Set_point                               = None
        self.Plc_Frecool_1_Differential_Set_Point_For_Start_Stop    = None
        self.Plc_CDZ_1_Differential_Set_Point_For_Start_Stop        = None
        
        self.Plc_Heater_1_Start_Set_Point                           = None
        self.Plc_Freecool_2_Start_Set_Point                         = None
        self.Plc_CDZ2_Start_Set_point                               = None
        self.Plc_Frecool_2_Differential_Set_Point_For_Start_Stop    = None
        self.Plc_CDZ_2_Differential_Set_Point_For_Start_Stop        = None
        self.Plc_Heater_2_Differential_Set_Point                    = None
        self.Plc_Heater_2_Start_Set_Point                           = None
        self.Plc_Heater_1_Differential_Set_Point                    = None
        self.Plc_Fan_1_Operating_Hours                              = None
        
        self.Plc_Compressor_1_Operating_Hours                       = None
        self.Plc_Fan_2_Operating_Hours                              = None
        self.Plc_Compressor_2_Operating_Hours                       = None
        self.Plc_Hours_And_Minute_To_Exchange_Set_Point             = None
        self.Plc_Emergency_Freecool_Temperature_Set_Point           = None
        self.Plc_Max_Speed_Temperature_Set_Point                    = None
        self.Plc_Min_Differential_External_Temperature_Set_Point    = None
        self.Plc_Max_External_Temperature_Threshould_Set_Point      = None
        self.Plc_Min_External_Temperature_Threshold_set_Point       = None

class Dc_Current_Measure():
    def __init__(self):
        self.BUS_Address    = CURRENT_TRANSFORMER_SLAVE_ADDRESS
        self.DC_Current = None
        self.DC_Voltage = None

        
class Measure():
    def __init__(self):
        self.ducati_measure     = Measure_Ducati()
        self.plc_measure        = Measure_Plc()
        self.dc_current_measure = Dc_Current_Measure()

class Site():
    def __init__(self):
        self.ID = 'RM194'
        self.measure = Measure()
        self.timestamp = None
        self.anno = None
        self.mese = None
        self.giorno = None
        self.ore = None
        self.minuti = None
        self.secondi = None


 
class ModbusController():
    
    debug = False;

    def ChangeSetpoint(self, command, value):
        plc = Modbus(PLC_SLAVE_ADDRESS, 'PLC SIEMENS', False)
        plc.Write_CMD(command, value, NUMBER_OF_WRITE_RETRY, 1)
    
    def DucatiInspector(self):
        ducati = Modbus(DUCATI_SLAVE_ADDRESS, 'DUCATI SMART', False)

    def CurrentTransformerInspector(self):        
        current_transformers = Modbus(CURRENT_TRANSFORMER_SLAVE_ADDRESS, 'CURRENT TRANSFORMER', False)
        

    def PLCInspector(self):
        plc = Modbus(PLC_SLAVE_ADDRESS, 'PLC SIEMENS', False)

    def InspectAllMeasures(self):
        ducati = Modbus(DUCATI_SLAVE_ADDRESS, 'DUCATI SMART', False)
        current_transformers = Modbus(CURRENT_TRANSFORMER_SLAVE_ADDRESS, 'CURRENT TRANSFORMER', False)
        plc = Modbus(PLC_SLAVE_ADDRESS, 'PLC SIEMENS', False)
        
        site = Site()
        
        site.timestamp                                                                  = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        site.anno                                                                       = datetime.datetime.fromtimestamp(time.time()).strftime('%Y')
        site.mese                                                                       = datetime.datetime.fromtimestamp(time.time()).strftime('%m')
        site.giorno                                                                     = datetime.datetime.fromtimestamp(time.time()).strftime('%d')
        site.ore                                                                        = datetime.datetime.fromtimestamp(time.time()).strftime('%H')
        site.minuti                                                                     = datetime.datetime.fromtimestamp(time.time()).strftime('%M')
        site.secondi                                                                    = datetime.datetime.fromtimestamp(time.time()).strftime('%S')
        site.measure.ducati_measure.AC_Voltage                                          = ducati.Get_Ducati_Three_Phase_Equivalent_Voltage()
        site.measure.ducati_measure.AC_Active_Power                                     = ducati.Get_Ducati_Active_Power()
        site.measure.ducati_measure.AC_Reactive_Power                                   = ducati.Get_Ducati_Reactive_Power()
        site.measure.ducati_measure.Power_Factor                                        = ducati.Get_Ducati_Power_Factor()
        site.measure.ducati_measure.Frequency                                           = ducati.Get_Ducati_Frequency()
        
        time.sleep(1)
        
        site.measure.plc_measure.Plc_External_Temperature                               = plc.Read_CMD(Plc_External_Temperature, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Internal_Temperature                               = plc.Read_CMD(Plc_Internal_Temperature, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Max_Internal_Temperature_Alarm_Set_Point           = plc.Read_CMD(Plc_Max_Internal_Temperature_Alarm_Set_Point, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Freecool_1_Start_Set_Point                         = plc.Read_CMD(Plc_Freecool_1_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)            
        site.measure.plc_measure.Plc_CDZ1_Start_Set_point                               = plc.Read_CMD(Plc_CDZ1_Start_Set_point, NUMBER_OF_READ_RETRY, 1)            
        site.measure.plc_measure.Plc_Frecool_1_Differential_Set_Point_For_Start_Stop    = plc.Read_CMD(Plc_Frecool_1_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_CDZ_1_Differential_Set_Point_For_Start_Stop        = plc.Read_CMD(Plc_CDZ_1_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Heater_1_Start_Set_Point                           = plc.Read_CMD(Plc_Heater_1_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Freecool_2_Start_Set_Point                         = plc.Read_CMD(Plc_Freecool_2_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_CDZ2_Start_Set_point                               = plc.Read_CMD(Plc_CDZ2_Start_Set_point, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Frecool_2_Differential_Set_Point_For_Start_Stop    = plc.Read_CMD(Plc_Frecool_2_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_CDZ_2_Differential_Set_Point_For_Start_Stop        = plc.Read_CMD(Plc_CDZ_2_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Heater_2_Differential_Set_Point                    = plc.Read_CMD(Plc_Heater_2_Differential_Set_Point, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Heater_2_Start_Set_Point                           = plc.Read_CMD(Plc_Heater_2_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Heater_1_Differential_Set_Point                    = plc.Read_CMD(Plc_Heater_1_Differential_Set_Point , NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Fan_1_Operating_Hours                              = plc.Read_CMD(Plc_Fan_1_Operating_Hours, NUMBER_OF_READ_RETRY, 0)
        site.measure.plc_measure.Plc_Compressor_1_Operating_Hours                       = plc.Read_CMD(Plc_Compressor_1_Operating_Hours, NUMBER_OF_READ_RETRY, 0)
        site.measure.plc_measure.Plc_Fan_2_Operating_Hours                              = plc.Read_CMD(Plc_Fan_2_Operating_Hours, NUMBER_OF_READ_RETRY, 0)
        site.measure.plc_measure.Plc_Compressor_2_Operating_Hours                       = plc.Read_CMD(Plc_Compressor_2_Operating_Hours, NUMBER_OF_READ_RETRY, 0)
        site.measure.plc_measure.Plc_Hours_And_Minute_To_Exchange_Set_Point             = plc.Read_CMD(Plc_Hours_And_Minute_To_Exchange_Set_Point, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Emergency_Freecool_Temperature_Set_Point           = plc.Read_CMD(Plc_Emergency_Freecool_Temperature_Set_Point, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Max_Speed_Temperature_Set_Point                    = plc.Read_CMD(Plc_Max_Speed_Temperature_Set_Point, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Min_Differential_External_Temperature_Set_Point    = plc.Read_CMD(Plc_Min_Differential_External_Temperature_Set_Point, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Max_External_Temperature_Threshould_Set_Point      = plc.Read_CMD(Plc_Max_External_Temperature_Threshould_Set_Point, NUMBER_OF_READ_RETRY, 1)
        site.measure.plc_measure.Plc_Min_External_Temperature_Threshold_set_Point       = plc.Read_CMD(Plc_Min_External_Temperature_Threshold_set_Point, NUMBER_OF_READ_RETRY, 1)
        
        time.sleep(1)
                  
        site.measure.dc_current_measure.DC_Current                                      = current_transformers.Read_CMD(Current_Transformer_RMS_Value_of_Current_A_x_100, NUMBER_OF_READ_RETRY, 2)
        site.measure.dc_current_measure.DC_Voltage                                      = 52
        
        json_data=json.dumps(site, default=j_default, indent=4, separators=(',', ': '))
        if self.debug==True:
            print(json_data)
        
        return json_data


"""
GENERAL_TEST_ENABLE = False
JSON_ENABLE = True

#SYSTEMS TEST

if (GENERAL_TEST_ENABLE):
    current_transformers.Current_Transformer_Test()
    time.sleep(1)
    plc.Plc_Test()
    time.sleep(1)
    ducati.Ducati_Test()
    time.sleep(1)

if(JSON_ENABLE):
    site = Site()
    
    site.timestamp                                                                  = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    site.anno                                                                       = datetime.datetime.fromtimestamp(time.time()).strftime('%Y')
    site.mese                                                                       = datetime.datetime.fromtimestamp(time.time()).strftime('%m')
    site.giorno                                                                     = datetime.datetime.fromtimestamp(time.time()).strftime('%d')
    site.ore                                                                        = datetime.datetime.fromtimestamp(time.time()).strftime('%H')
    site.minuti                                                                     = datetime.datetime.fromtimestamp(time.time()).strftime('%M')
    site.secondi                                                                    = datetime.datetime.fromtimestamp(time.time()).strftime('%S')
    site.measure.ducati_measure.AC_Voltage                                          = ducati.Get_Ducati_Three_Phase_Equivalent_Voltage()
    site.measure.ducati_measure.AC_Active_Power                                     = ducati.Get_Ducati_Active_Power()
    site.measure.ducati_measure.AC_Reactive_Power                                   = ducati.Get_Ducati_Reactive_Power()
    site.measure.ducati_measure.Power_Factor                                        = ducati.Get_Ducati_Power_Factor()
    site.measure.ducati_measure.Frequency                                           = ducati.Get_Ducati_Frequency()
    
    time.sleep(1)
    
    site.measure.plc_measure.Plc_External_Temperature                               = plc.Read_CMD(Plc_External_Temperature, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Internal_Temperature                               = plc.Read_CMD(Plc_Internal_Temperature, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Max_Internal_Temperature_Alarm_Set_Point           = plc.Read_CMD(Plc_Max_Internal_Temperature_Alarm_Set_Point, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Freecool_1_Start_Set_Point                         = plc.Read_CMD(Plc_Freecool_1_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)            
    site.measure.plc_measure.Plc_CDZ1_Start_Set_point                               = plc.Read_CMD(Plc_CDZ1_Start_Set_point, NUMBER_OF_READ_RETRY, 1)            
    site.measure.plc_measure.Plc_Frecool_1_Differential_Set_Point_For_Start_Stop    = plc.Read_CMD(Plc_Frecool_1_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_CDZ_1_Differential_Set_Point_For_Start_Stop        = plc.Read_CMD(Plc_CDZ_1_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Heater_1_Start_Set_Point                           = plc.Read_CMD(Plc_Heater_1_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Freecool_2_Start_Set_Point                         = plc.Read_CMD(Plc_Freecool_2_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_CDZ2_Start_Set_point                               = plc.Read_CMD(Plc_CDZ2_Start_Set_point, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Frecool_2_Differential_Set_Point_For_Start_Stop    = plc.Read_CMD(Plc_Frecool_2_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_CDZ_2_Differential_Set_Point_For_Start_Stop        = plc.Read_CMD(Plc_CDZ_2_Differential_Set_Point_For_Start_Stop, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Heater_2_Differential_Set_Point                    = plc.Read_CMD(Plc_Heater_2_Differential_Set_Point, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Heater_2_Start_Set_Point                           = plc.Read_CMD(Plc_Heater_2_Start_Set_Point, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Heater_1_Differential_Set_Point                    = plc.Read_CMD(Plc_Heater_1_Differential_Set_Point , NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Fan_1_Operating_Hours                              = plc.Read_CMD(Plc_Fan_1_Operating_Hours, NUMBER_OF_READ_RETRY, 0)
    site.measure.plc_measure.Plc_Compressor_1_Operating_Hours                       = plc.Read_CMD(Plc_Compressor_1_Operating_Hours, NUMBER_OF_READ_RETRY, 0)
    site.measure.plc_measure.Plc_Fan_2_Operating_Hours                              = plc.Read_CMD(Plc_Fan_2_Operating_Hours, NUMBER_OF_READ_RETRY, 0)
    site.measure.plc_measure.Plc_Compressor_2_Operating_Hours                       = plc.Read_CMD(Plc_Compressor_2_Operating_Hours, NUMBER_OF_READ_RETRY, 0)
    site.measure.plc_measure.Plc_Hours_And_Minute_To_Exchange_Set_Point             = plc.Read_CMD(Plc_Hours_And_Minute_To_Exchange_Set_Point, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Emergency_Freecool_Temperature_Set_Point           = plc.Read_CMD(Plc_Emergency_Freecool_Temperature_Set_Point, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Max_Speed_Temperature_Set_Point                    = plc.Read_CMD(Plc_Max_Speed_Temperature_Set_Point, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Min_Differential_External_Temperature_Set_Point    = plc.Read_CMD(Plc_Min_Differential_External_Temperature_Set_Point, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Max_External_Temperature_Threshould_Set_Point      = plc.Read_CMD(Plc_Max_External_Temperature_Threshould_Set_Point, NUMBER_OF_READ_RETRY, 1)
    site.measure.plc_measure.Plc_Min_External_Temperature_Threshold_set_Point       = plc.Read_CMD(Plc_Min_External_Temperature_Threshold_set_Point, NUMBER_OF_READ_RETRY, 1)
    
    time.sleep(1)
              
    site.measure.dc_current_measure.DC_Current                                      = current_transformers.Read_CMD(Current_Transformer_RMS_Value_of_Current_A_x_100, NUMBER_OF_READ_RETRY, 2)
    site.measure.dc_current_measure.DC_Voltage                                      = 52
    
    print(json.dumps(site, default=j_default, indent=4, separators=(',', ': ')))

"""


