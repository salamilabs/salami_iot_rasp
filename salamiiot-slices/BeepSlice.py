import logging

from decorators import consumes
from model.action import SalamiAction
from slice import ABSSlice

SLICE_SLOT = 5


class BeepSlice(ABSSlice):
    def __init__(self, slice_slot):
        super().__init__(slice_slot)
        self.__logger = logging.getLogger(self.__class__.__name__)

    @staticmethod
    def beep():
        print('\a')

    @consumes(SalamiAction)
    def on_message(self, message):
        BeepSlice.beep()


if __name__ == "__main__":
    beep_slice = BeepSlice(SLICE_SLOT)
    beep_slice.activate()
