import array
import logging
import time

import serial

import SalamiConstants
from slice import ABSSlice

SLICE_SLOT = 4
SERIAL_MESSAGE_LENGTH = 9


class SerialSlice(ABSSlice):
    def __init__(self, in_port, baudrate, serial_message_length, slice_slot):
        super().__init__(slice_slot)
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.info("***", "Initializing SerialSlice", "***")
        self._logger.info("RUNNING SETTINGS")
        self._logger.info("IN PORT", in_port, "BAUDRATE", baudrate)
        self._logger.info("***************************")
        # SERIAL
        self.in_port = in_port
        self.serial_in = None
        self.serial_in_enabled = False
        self.baudrate = baudrate
        self.serial_message_length = serial_message_length
        self.serial_data = None

        self.set_up_comm_ports(self.baudrate)

        if self.serial_in_enabled:
            self._logger.info("SerialSlice ready!")
        else:
            self._logger.error("No SerialSlice Hardware found. Program exit.")
            exit()
        time.sleep(0.5)

    def set_up_comm_ports(self, baudrate):
        self._logger.info("Setting up COM Ports. With baudrate: %s", str(baudrate))
        self.open_serial_in(baudrate)

    def open_serial_in(self, baudrate):
        try:
            self._logger.info("Opening SerialSlice Ingresso on port: %s", self.in_port)
            self.serial_in = serial.Serial(
                port=self.in_port,
                baudrate=baudrate,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS
            )
            self.serial_in_enabled = True
        except Exception as err:
            print("Error Opening Serial Port. Disabling port.", err)
            self.serial_in_enabled = False

    @staticmethod
    def receive_data(ser):
        packet = array.array('B', [])
        if ser.inWaiting():
            while ser.inWaiting():
                single_byte = ord(ser.read(1))
                packet.append(single_byte)
            return packet
        else:
            return None

    @staticmethod
    def execute_command(ser, command, sleep_time):
        ser.write("".join(chr(i) for i in command))
        time.sleep(sleep_time)
        response = SerialSlice.receive_data(ser)
        return response

    def read(self):
        if self.serial_in_enabled:
            self.serial_data = SerialSlice.receive_data(self.serial_in)
        if self.serial_data != '' and self.serial_data is not None:
            return int(self.serial_data[0])
        else:
            return -1

    def send_ingredient_update(self):
        while 1:
            button_pressed = self.read()
            if button_pressed != -1:
                self._iot.update_ingredient(SalamiConstants.CMD_SEND_DATA_EVENT_AC,
                                            "BUTTON" + str(button_pressed), 1)
            time.sleep(0.1)


if __name__ == '__main__':
    sl = SerialSlice("COM3", 19200, 1, SLICE_SLOT)
    sl.activate()
    sl.send_ingredient_update()
