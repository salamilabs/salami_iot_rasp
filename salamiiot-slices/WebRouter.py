import logging

from flask import Flask
from flask.ext.cors import CORS
from flask.ext.restful import reqparse
from flask_restful import Resource, Api


class Measures(Resource):
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('maxInternalTemp')
        self.parser.add_argument('freecool1Start')
        self.parser.add_argument('freecool2Start')
        self.parser.add_argument('conditioner1Start')
        self.parser.add_argument('conditioner2Start')
        self.parser.add_argument('maxExtTempThres')
        self.parser.add_argument('minExtTempThres')

    def get(self):
        measure = {'maxInternalTemp': 25.5,
                   'freecool1Start': 30,
                   'freecool2Start': 30,
                   'conditioner1Start': 35,
                   'conditioner2Start': 35,
                   'maxExtTempThres': 50,
                   'minExtTempThres': 40}
        return measure

    def post(self):
        args = self.parser.parse_args()
        self._logger.info("maxInternalTemp %s", args['maxInternalTemp'])
        self._logger.info("freecool1Start %s", args['freecool1Start'])
        self._logger.info("freecool2Start %s", args['freecool2Start'])
        self._logger.info("conditioner1Start %s", args['conditioner1Start'])
        self._logger.info("conditioner2Start %s", args['conditioner2Start'])
        self._logger.info("maxExtTempThres %s", args['maxExtTempThres'])
        self._logger.info("minExtTempThres %s", args['minExtTempThres'])


if __name__ == '__main__':
    log_format = "%(levelname) -10s %(asctime)s %(module)s:%(lineno)s %(funcName)s %(message)s"
    logging.basicConfig(format=log_format, level=logging.INFO)
    app = Flask(__name__)
    CORS(app)
    api = Api(app)
    api.add_resource(Measures, '/measures')
    app.run(debug=True)
