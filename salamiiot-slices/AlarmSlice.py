import logging
import time

import RPi.GPIO as GPIO

import SalamiConstants
from decorators import consumes
from model.action import SalamiAction
from slice import ABSSlice

SLICE_SLOT = 2

SEM_R = 22
SEM_V = 23
sem_v_rele = 27

BUZZER = 17
LOCK = 18

DOOR_SENSOR = 24
TEMP_SENSOR = 25


class AlarmSlice(ABSSlice):
    def __init__(self, slice_slot):
        super().__init__(slice_slot)
        self._logger = logging.getLogger(self.__class__.__name__)

        try:
            '''
            USCITE
            '''
            GPIO.setmode(GPIO.BCM)
            # Semaforo Rosso
            GPIO.setup(SEM_R, GPIO.OUT)
            # Semaforo Verde
            GPIO.setup(SEM_V, GPIO.OUT)
            # Semaforo Verde Rele
            GPIO.setup(sem_v_rele, GPIO.OUT)
            # Buzzer
            GPIO.setup(BUZZER, GPIO.OUT)
            # Serratura
            GPIO.setup(LOCK, GPIO.OUT)
            '''
            INGRESSI
            '''
            # SENSORE PORTA
            GPIO.setup(DOOR_SENSOR, GPIO.IN)
            # SENSORE_TEMPERATURA
            GPIO.setup(TEMP_SENSOR, GPIO.IN)
            '''
            INIZIALIZZAZIONI USCITE
            '''
            # Semaforo Rosso
            GPIO.output(SEM_R, 1)
            # Semaforo Verde
            GPIO.output(SEM_V, 0)
            # Semaforo Verde RL
            GPIO.output(sem_v_rele, 0)
            # Buzzer
            GPIO.output(BUZZER, 0)
            # Serratura
            GPIO.output(LOCK, 0)

        except Exception:
            self._logger.exception("ERROR - GPIO initialization")

        self._logger.info("Alarm Slice correctly initialized")

    def send_ingredient_update(self):
        self.DOOR_ALARM=0;
        self.TEMP_ALARM=0;
        
        
        while True:
            if self.get_door_alarm() == 'ALLARME_ON' and self.DOOR_ALARM==0:
                self._iot.update_ingredient(SalamiConstants.CMD_SEND_DATA_EVENT_AC, "DOOR_ALARM", 1)
                self.DOOR_ALARM=1
                print(" *************** DOOR_ALARM 1")
            
            if self.get_door_alarm() == 'ALLARME_OFF' and self.DOOR_ALARM==1:
                self._iot.update_ingredient(SalamiConstants.CMD_SEND_DATA_EVENT_AC, "DOOR_ALARM", 0)
                self.DOOR_ALARM=0
                print(" *************** DOOR_ALARM 0")
            
            
            if self.get_temperature_alarm() == 'ALLARME_ON' and self.TEMP_ALARM==0:
                self._iot.update_ingredient(SalamiConstants.CMD_SEND_DATA_EVENT_AC, "TEMP_ALARM", 1)
                self.TEMP_ALARM=1;
                print(" *************** TEMP_ALARM 1")
                
            if self.get_temperature_alarm() == 'ALLARME_OFF' and self.TEMP_ALARM==1:
                self._iot.update_ingredient(SalamiConstants.CMD_SEND_DATA_EVENT_AC, "TEMP_ALARM", 0)
                self.TEMP_ALARM=0;
                print(" *************** TEMP_ALARM 0")

            time.sleep(1)

    @consumes(SalamiAction)
    def on_message(self, message):
        if message is not None:
            action_name = message.name
            # select what to do on the basis of action_request.name VALUE
            # print "action_name: " + action_name
            if action_name == "BUZZER_ON":
                if message.is_gpio_action():
                    self.buzzer_on()
            if action_name == "BUZZER_OFF":
                if message.is_gpio_action():
                    self.buzzer_off()

    def set_semaphore(self, value):
        try:
            GPIO.output(SEM_V, value)
            GPIO.output(sem_v_rele, value)

        except Exception:
            self._logger.exception("ERROR - Setting semaphore failed ")

    def get_door_alarm(self):
        try:
            if GPIO.input(DOOR_SENSOR) == 1:
                return "ALLARME_ON"
            else:
                return "ALLARME_OFF"
        except Exception:
            self._logger.exception("ERROR - Getting door alarm failed ")

    def get_temperature_alarm(self):
        try:
            if GPIO.input(TEMP_SENSOR) == 1:
                return "ALLARME_ON"
            else:
                return "ALLARME_OFF"
        except Exception:
            self._logger.exception("ERROR - Get temperature alarm failed")

    def alarm(self):
        try:
            for i in range(0, 10):
                GPIO.output(BUZZER, 1)
                time.sleep(0.1)
                GPIO.output(BUZZER, 0)
                time.sleep(0.1)
        except Exception:
            self._logger.exception("ERROR - Sending alarm failed")

    def buzzer_on(self):
        try:
            GPIO.output(BUZZER, 1)
            time.sleep(0.1)
        except Exception:
            self._logger.exception("ERROR - Activating the buzzer failed")

    def buzzer_off(self):
        try:
            GPIO.output(BUZZER, 0)
            time.sleep(0.1)
        except Exception:
            self._logger.exception("ERROR - Deactivating the buzzer failed ")

    def set_lock(self, value):
        try:
            GPIO.output(LOCK, value)
        except Exception:
            self._logger.exception("ERROR - Setting lock failed %s")

    def start_sequence(self):
        try:
            GPIO.output(BUZZER, 1)
            time.sleep(0.8)
            GPIO.output(BUZZER, 0)
            time.sleep(0.2)
            GPIO.output(BUZZER, 1)
            time.sleep(0.8)
            GPIO.output(BUZZER, 0)
            time.sleep(0.2)
            GPIO.output(BUZZER, 1)
            time.sleep(0.8)
            GPIO.output(BUZZER, 0)
            time.sleep(0.2)
        except Exception:
            self._logger.exception("Starting the sequence failed")


if __name__ == "__main__":
    alarm = AlarmSlice(SLICE_SLOT)
    alarm.activate()
    alarm.send_ingredient_update()
