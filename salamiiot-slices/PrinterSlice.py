import logging
import os

from decorators import consumes
from model.action import SalamiAction
from slice import ABSSlice

SLICE_SLOT = 2


class PrinterSlice(ABSSlice):
    def __init__(self, slice_slot):
        super().__init__(slice_slot)
        self._logger = logging.getLogger(self.__class__.__name__)

    def print(self, field1, field2):
        try:
            # Stampa LOGO
            os.system("echo '\x1d\x70\x01\\' > /dev/usb/lp0")
            # Centro
            os.system("echo '\x1b\x61\x01' > /dev/usb/lp0")
            # Normal
            os.system("echo '\x1b\x45\\' > /dev/usb/lp0")
            os.system("echo 'SALAMI IoT' > /dev/usb/lp0")
            # Centro
            os.system("echo '\x1b\x61\x01' > /dev/usb/lp0")
            # Bold
            os.system("echo '\x1b\x45\x01' > /dev/usb/lp0")
            os.system("echo " + field1 + " > /dev/usb/lp0")
            # Normal
            os.system("echo '\x1b\x45\\' > /dev/usb/lp0")
            os.system("echo " + field2 + " > /dev/usb/lp0")

            # Allinea Sinistra
            os.system("echo '\x1b\x61\x48' > /dev/usb/lp0")
            # NORMAL
            os.system("echo '\x1b\x45\\' > /dev/usb/lp0")
            # Taglia Carta
            os.system("echo '\x1b\x69' > /dev/usb/lp0")

        except Exception as err:
            self._logger.info("Errore su stampante" + str(err))

    @consumes(SalamiAction)
    def on_message(self, message):
        if message is not None:
            field1 = message.get_dataload_field("Text1")
            field2 = message.get_dataload_field("text2")
            self._logger.info("Action name is: %s", message.name)
            self.print(field1, field2)
            self._logger.info(field1)
            self._logger.info(field2)

if __name__ == '__main__':
    printer_slice = PrinterSlice(SLICE_SLOT)
    printer_slice.activate()
