import array
import binascii
import threading
import time
from queue import Queue

import serial
# vedere modifiche da fare in read tags e aggiungere self.flushqueue ogni volta che parte una nuova lettura!
import SalamiConstants
from kitchen import IOTKitchen


class ReaderCommand:
    @staticmethod
    def cmd_fast_switch_ant_inventory(reader_address, ant_1, ant_1_round, ant_2, ant_2_round, ant_3, ant_3_round, ant_4,
                                      ant_4_round, switching_pause, repeat):
        cmd = 0x8A
        impinji_cmd = [0xA0, 0x0D, reader_address, cmd, ant_1, ant_1_round, ant_2, ant_2_round, ant_3, ant_3_round,
                       ant_4, ant_4_round, switching_pause, repeat]
        crc = Checksum.calc(impinji_cmd)
        impinji_cmd.extend([crc])
        # print "CMD:",binascii.hexlify(array.array('B',impinji_cmd))
        return impinji_cmd

    @staticmethod
    def cmd_set_work_antenna(reader_address, antenna):
        cmd = 0x74
        impinji_cmd = [0xA0, 0x04, reader_address, cmd, antenna]
        crc = Checksum.calc(impinji_cmd)
        impinji_cmd.extend([crc])
        # print "CMD:",binascii.hexlify(array.array('B',impinji_cmd))
        return impinji_cmd

    @staticmethod
    def cmd_set_power(reader_address, power):
        # A0 04 01 76 1E C7
        cmd = 0x76
        impinji_cmd = [0xA0, 0x04, reader_address, cmd, power]
        crc = Checksum.calc(impinji_cmd)
        impinji_cmd.extend([crc])
        print("CMD:", binascii.hexlify(array.array('B', impinji_cmd)))
        return impinji_cmd

    @staticmethod
    def cmd_get_power(reader_address):
        # A0 04 01 76 1E C7
        cmd = 0x77
        impinji_cmd = [0xA0, 0x03, reader_address, cmd]
        crc = Checksum.calc(impinji_cmd)
        impinji_cmd.extend([crc])
        print("CMD:", binascii.hexlify(array.array('B', impinji_cmd)))
        return impinji_cmd

    @staticmethod
    def cmd_tag_inventory_real_time_mode(reader_address, repeat):
        cmd = 0x89
        impinji_cmd = [0xA0, 0x04, reader_address, cmd, repeat]
        crc = Checksum.calc(impinji_cmd)
        impinji_cmd.extend([crc])
        print("CMD:", binascii.hexlify(array.array('B', impinji_cmd)))
        return impinji_cmd


class Checksum:
    @staticmethod
    def verify(byte_array):
        # print "verify checksum of ",byte_array
        tmp_sum = 0x00
        # nell'ultimo byte  custodito il CHECKSUM
        checksum = byte_array[byte_array.__len__() - 1]
        # print "Checksum received:",checksum
        byte_array = byte_array[:byte_array.__len__() - 1]
        # print "Calculate on ",byte_array
        # SALTO IL PREAMBLE FF e il CHECK SUM
        for byte in byte_array:
            tmp_sum = tmp_sum + byte
        tmp_sum = (((~tmp_sum) + 1) & 255)
        # print "calculated checksum",tmp_sum
        if checksum == tmp_sum:  # riporto il numero a 1 BYTE (8 bit)
            # print "CHECKSUM OK"
            return True
        print("CHECKSUM FAILED")
        return False

    @staticmethod
    def calc(byte_array):
        tmp_sum = 0x00
        for byte in byte_array:
            tmp_sum = tmp_sum + byte
        return ((~tmp_sum) + 1) & 255  # riporto il numero a 1 BYTE (8 bit)


class Antenna(threading.Thread):
    tag_counter = 0
    response_counter = 0
    logger = None
    leggi = False
    kermit = None
    tags_inventory = []  # sono i TAG letti nella transazione
    last_transaction_counter = 0
    total_counter = 0
    wrong_counter = 0
    commandQueue = None
    tagQueue = None
    i = 0

    @staticmethod
    def get_instance():
        if Antenna.instance is None:
            Antenna.instance = Antenna()
        return Antenna.instance

    def __init__(self):
        threading.Thread.__init__(self)
        # FAST SWITCHING
        self.read_command = ReaderCommand.cmd_fast_switch_ant_inventory(0xFF, 0x01, 0x01, 0x01, 0x02, 0x01, 0x03, 0x01,
                                                                        0x04, 0x01, 0x1)
        # INVENTORY REAL TIME
        self.set_work_antenna = ReaderCommand.cmd_set_work_antenna(0xFF, 0x00)
        self.read_command_real_time = ReaderCommand.cmd_tag_inventory_real_time_mode(0xFF, 0x01)

        self.commandQueue = Queue()
        self.tagQueue = Queue()
        self.open_serial()
        self.ser = None
        self.iot = None
        self.instance = Antenna

    def open_serial(self):
        self.ser = serial.Serial(
                port='COM12',
                # port='/dev/ttyS1',
                baudrate=115200,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=0.1
        )

    def receive_data(self):
        begin_of_packet = False
        read_tags = []
        packet = array.array('B', [])
        if self.ser.inWaiting():
            while self.ser.inWaiting():
                single_byte = ord(self.ser.read(1))
                if (single_byte == 160) and (begin_of_packet is False):
                    packet.append(single_byte)
                    # lunghezza
                    packet_length = ord(self.ser.read(1))

                    # ..........DA RIMUOVERE..............
                    if packet_length == 19:
                        self.tag_counter += 1
                    # ..........DA RIMUOVERE..............

                    packet.append(packet_length)
                    pending_bytes = self.ser.inWaiting()
                    # print "Pending Bytes:", pending_bytes
                    # print "Packet Lenght:",packet_length
                    if packet_length <= pending_bytes:
                        i = 0
                        while i < packet_length:
                            packet.append(ord(self.ser.read(1)))
                            i += 1
                        begin_of_packet = False
                        parse_result = self.parse_packet(packet)
                        if parse_result is not None:
                            read_tags.append(parse_result)
                        packet = array.array('B', [])
                    else:
                        print("Packet Error")
                        return None
                else:
                    return None
            return read_tags

    @staticmethod
    def parse_packet(packet):
        if packet[3] == 137:  # 0x89 CMD_REAL_TIME_INVENTORY
            if packet[1] == 4:  # 0x04 -> ERROR
                print("Error. Code:", hex(packet[4]))
                return None
            if packet[1] == 10:  # 0x04 -> SUCCESS
                return None
            if packet[1] > 10:  # e' un EPC
                # EPC e' memorizzato dalla posizione 8 (inclusa) fino alla fine del paccketto -2 (dove sono RSSI
                # e CHECKSUM)
                epc = packet[7: -2]
                epc_hex = binascii.hexlify(epc)
                print("EPC:", epc_hex)
                return epc_hex

    @staticmethod
    def create_command(lenght, command, data):
        # crea il command
        # calcola il CRC
        # restituisce i dati da mandare alla serial
        preamble = array.array('B', [0xFF])
        impinji_cmd = []
        soc = [0x00]  # Start of command - reserved byte
        impinji_cmd.extend(preamble)
        impinji_cmd.extend(soc)
        impinji_cmd.extend(lenght)
        impinji_cmd.extend(command)
        impinji_cmd.extend(data)
        command_crc = Checksum.calc(impinji_cmd)
        # print command_crc
        impinji_cmd.extend([command_crc])
        # print sonmicro_cmd
        print("CMD:", binascii.hexlify(array.array('B', impinji_cmd)))
        return impinji_cmd

    def execute_command(self, command, sleep_time):
        self.response_counter += 1
        self.ser.write("".join(chr(i) for i in command))
        time.sleep(sleep_time)

    def receive_command_response(self):
        response = self.receive_data()
        return response

    def execute_read_command(self, command_type):
        result = None
        if command_type == "SINGLE_REAL_TIME":
            self.execute_command(self.set_work_antenna, 0.05)
            self.receive_command_response()
            self.execute_command(self.read_command_real_time, 0.05)
            result = self.receive_command_response()
        return result

    def read_tags(self):
        try:
            result = self.execute_read_command("SINGLE_REAL_TIME")
            return result
        except Exception as run_exception:
            print("Eccezione in READ_TAGS() nel thread Antenna", str(run_exception))

    def run(self):
        self.iot.connect()
        while True:

            result = self.read_tags()

            if (result is not None) and (result != []):
                if result[0] == "e2003412dc03011945203221":
                    self.iot.notifyIngrediendUpdate("ELMETTO", "1")
                if result[0] == "540100000000000000000002":
                    self.iot.notifyIngrediendUpdate("SCARPE", "1")

            action_request = self.iot.getMessage()

            if action_request is not None:
                if action_request.isGPIOAction():
                    repeat = action_request.get_repeat()
                    i = 0
                    while i < int(repeat):
                        print("I should repeat for : " + str(repeat))
                        i += 1
                        # self.SEND_SOCKET_DATA("192.168.1.129", 1972, "AUX1ON")
                        print(action_request.name + str(i))

                if action_request.name == "SMS_ACTION":
                    # self.SEND_SOCKET_DATA("192.168.1.129", 1987, "STAMPA")
                    print("SMS_ACTION")
                    # print action


# print sys.path

rfidlice = Antenna.get_instance()
rfidlice.iot = IOTKitchen("http://192.168.1.129/SalamiBoard", 1,
                                SalamiConstants.POLLING_BEHAVIOR_MANIAC, 1, True)
rfidlice.start()
