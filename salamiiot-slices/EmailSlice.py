import logging
import os
import smtplib
import time
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from decorators import consumes
from model.action import SalamiAction
from slice import ABSSlice

SLICE_SLOT = 1

PHOTO_NAME = 'Snapshot.jpg'
PHOTO_URL = 'http://www.ilcantierehitech.it/Galata/Galata_Logo.jpg'


class EmailSlice(ABSSlice):
    def __init__(self, slice_slot):
        super().__init__(slice_slot)
        self.__logger = logging.getLogger(self.__class__.__name__)
        self._iot = None
        self._smtp_server = 'smtp.securcantieri.com'
        self.sender = 'valerio.sinisi@securcantieri.com'
        self.username = 'valerio.sinisi@securcantieri.com'
        self.password = 'puttanella'

    @consumes(SalamiAction)
    def on_message(self, message):
        if message is not None:
            action_name = message.name
            # select what to do on the basis of action_request.name VALUE
            if action_name == "EMAIL":
                address = message.get_dataload_field("Address")
                subject = message.get_dataload_field("Subject")
                body = message.get_dataload_field("Text")
                self.send_email(address, subject, body)
                self.__logger.info("Email correctly sent")

            if action_name == "SNAPSHOT":
                address = message.get_dataload_field("Address")
                subject = message.get_dataload_field("Subject")
                body = message.get_dataload_field("Text")
                time.sleep(2)
                self.send_snapshot(address, subject, body)
                self.__logger.info("Email with snapshot correctly sent")

    def send_email(self, address, subject, body):

        # subject="SALAMI LABS"
        destination = [address]

        # SALAMI_IOT_IMAGE:  http://www.ilcantierehitech.it/SalamiTOIKitchen/images/LogoSalamiIOT.png
        content_html = """\
        <html>
        <head></head>
        <img src="http://www.ilcantierehitech.it/Galata/Galata_Logo.jpg"><p>"""

        content_html = content_html + body

        content_html += " </p></html>"

        try:
            msg = MIMEMultipart()
            msg['Subject'] = subject
            msg['From'] = self.sender
            msg['To'] = address

            image = MIMEText(content_html, 'html')  # html

            msg.attach(image)

            conn = smtplib.SMTP(self._smtp_server, 587)
            conn.ehlo()
            conn.starttls()
            conn.ehlo()
            conn.set_debuglevel(False)
            conn.login(self.username, self.password)
            try:
                conn.sendmail(self.sender, destination, msg.as_string())
                self.__logger.debug("Email sent to server")

            finally:
                conn.close()
        except Exception:
            self.__logger.exception("Error occurred while sending the email: ")

    def send_snapshot(self, address, subject, body):

        destination = [address]
        content_html = """\
        <html>
        <head></head>
        <img src="http://www.ilcantierehitech.it/Galata/Galata_Logo.jpg"><p>"""

        content_html = content_html + body

        content_html += " </p></html>"

        try:
            os.system('fswebcam -d /dev/video0 -r 640x480 ' + PHOTO_NAME)
            self.__logger.info("Photo taken")
        except Exception:
            self.__logger.exception("Error occured while taking photo: ")

        try:

            msg = MIMEMultipart()
            msg['Subject'] = subject
            msg['From'] = self.sender
            msg['To'] = address

            photo = None
            try:
                fp = open(PHOTO_NAME, 'rb')
                photo = MIMEImage(fp.read())
                fp.close()
            except Exception:
                self.__logger.exception("Error occurred while opening the photo file")

            try:

                photo.add_header('Content-Disposition', 'attachment', filename=PHOTO_NAME)
                msg.attach(photo)

            except Exception:
                self.__logger.exception("Error while attaching the photo ")

            image = MIMEText(content_html, 'html')
            msg.attach(image)

            conn = smtplib.SMTP(self._smtp_server)
            conn.ehlo()
            conn.starttls()
            conn.ehlo()
            conn.set_debuglevel(False)
            conn.login(self.username, self.password)

            try:
                conn.sendmail(self.sender, destination, msg.as_string())
                self.__logger.info("Email sent")

            finally:
                conn.close()
        except Exception as exc:
            self.__logger.error("Error occurred while sending the email %s", exc)


if __name__ == "__main__":
    email_slice = EmailSlice(SLICE_SLOT)
    email_slice.activate()
