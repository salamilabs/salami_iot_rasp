import logging

import SalamiConstants
from decorators import sleep_after
from slice import ABSSlice

SLICE_SLOT = 4
COMMAND_NAME = "BUTTON6"
COMMAND_VALUE = 1


class DummySlice(ABSSlice):
    def __init__(self, slice_slot):
        super().__init__(slice_slot)
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.info("**************************************")
        self._logger.info("***    Initializing DummySlice     ***")
        self._logger.info("**************************************")

    @sleep_after(1)
    def _send_ingredient(self):
        self._iot.update_ingredient(SalamiConstants.CMD_SEND_DATA_EVENT_AC, COMMAND_NAME, COMMAND_VALUE)

    def send_ingredient_update(self):
        for x in range(0, 5000):
            self._send_ingredient()

        self.deactivate()

    def on_message(self, message):
        pass


if __name__ == '__main__':
    dummy = DummySlice(SLICE_SLOT)
    dummy.activate()
    dummy.send_ingredient_update()
