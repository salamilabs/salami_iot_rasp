import logging

from flask import Flask
from flask.ext.cors import CORS
from flask.ext.restful import Api, Resource
from flask.ext.restful import reqparse

import ModbusLogic
from ModbusLogic import ModbusController
from decorators import consumes
from model.action import SalamiAction
from slice import ABSSlice

MEASURE_REFRESH_INTERVAL = 10000  # in milliseconds


class ModbusSlice(ABSSlice):
    modbus_controller = None

    def __init__(self, slice_slot):
        super().__init__(slice_slot)
        self._logger = logging.getLogger(self.__class__.__name__)

        try:
            self.modbus_controller = ModbusController()
            pass
        except Exception:
            self._logger.exception("ERROR - slice initialization")

        self._logger.info("Modbus Slice correctly initialized")

    def send_ingredient_update(self):

        json_data = self.modbus_controller.InspectAllMeasures()
        '''
        if self.get_door_alarm() == 'ALLARME_ON' and self.DOOR_ALARM==0:
            self._iot.update_ingredient(SalamiConstants.CMD_SEND_DATA_EVENT_AC, "DOOR_ALARM", 1)
            self.DOOR_ALARM=1
            print(" *************** DOOR_ALARM 1")
        '''
        return json_data

    def change_setpoint(self, command, value):
        self.modbus_controller.ChangeSetpoint(command, value)

    @consumes(SalamiAction)
    def on_message(self, message):
        pass


class Measures(Resource):
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('Plc_Max_Internal_Temperature_Alarm_Set_Point')
        self.parser.add_argument('Plc_Freecool_1_Start_Set_Point')
        self.parser.add_argument('Plc_Freecool_2_Start_Set_Point')
        self.parser.add_argument('Plc_CDZ1_Start_Set_point')
        self.parser.add_argument('Plc_CDZ2_Start_Set_point')
        self.parser.add_argument('Plc_Max_External_Temperature_Threshould_Set_Point')
        self.parser.add_argument('Plc_Min_External_Temperature_Threshold_set_Point')

    def get(self):
        return modbus_slice.send_ingredient_update()

    def post(self):
        args = self.parser.parse_args()
        max_int_temp = args['Plc_Max_Internal_Temperature_Alarm_Set_Point']
        free_cool1 = args['Plc_Freecool_1_Start_Set_Point']
        free_cool2 = args['Plc_Freecool_2_Start_Set_Point']
        cond1 = args['Plc_CDZ1_Start_Set_point']
        cond2 = args['Plc_CDZ2_Start_Set_point']
        ext_max_temp_thres = args['Plc_Max_External_Temperature_Threshould_Set_Point']
        ext_min_temp_thres = args['Plc_Min_External_Temperature_Threshold_set_Point']
        self._logger.debug("Plc_Max_Internal_Temperature_Alarm_Set_Point %s", max_int_temp)
        self._logger.debug("Plc_Freecool_1_Start_Set_Point %s", free_cool1)
        self._logger.debug("Plc_Freecool_2_Start_Set_Point %s", free_cool2)
        self._logger.debug("Plc_CDZ1_Start_Set_point %s", cond1)
        self._logger.debug("Plc_CDZ2_Start_Set_point %s", cond2)
        self._logger.debug("Plc_Max_External_Temperature_Threshould_Set_Point %s", ext_max_temp_thres)
        self._logger.debug("Plc_Min_External_Temperature_Threshold_set_Point %s", ext_min_temp_thres)

        if max_int_temp is not None:
            print(max_int_temp)
            modbus_slice.change_setpoint(ModbusLogic.Plc_Max_Internal_Temperature_Alarm_Set_Point, float(max_int_temp))
        if free_cool1 is not None:
            modbus_slice.change_setpoint(ModbusLogic.Plc_Freecool_1_Start_Set_Point, float(free_cool1))
        if free_cool2 is not None:
            modbus_slice.change_setpoint(ModbusLogic.Plc_Freecool_2_Start_Set_Point, float(free_cool2))
        if cond1 is not None:
            modbus_slice.change_setpoint(ModbusLogic.Plc_CDZ1_Start_Set_point, float(cond1))
        if cond2 is not None:
            modbus_slice.change_setpoint(ModbusLogic.Plc_CDZ2_Start_Set_point, float(cond2))
        if ext_max_temp_thres is not None:
            modbus_slice.change_setpoint(ModbusLogic.Plc_Max_External_Temperature_Threshould_Set_Point,
                                         float(ext_max_temp_thres))
        if ext_min_temp_thres is not None:
            modbus_slice.change_setpoint(ModbusLogic.Plc_Min_External_Temperature_Threshold_set_Point,
                                         float(ext_min_temp_thres))


SLICE_SLOT = 3
modbus_slice = ModbusSlice(SLICE_SLOT)

if __name__ == "__main__":
    log_format = "%(levelname) -10s %(asctime)s %(module)s:%(lineno)s %(funcName)s %(message)s"
    logging.basicConfig(format=log_format, level=logging.DEBUG, filename="/opt/salamiiot/modbus_flask.log")
    app = Flask(__name__)
    CORS(app)
    api = Api(app)
    api.add_resource(Measures, '/measures')
    app.run(debug=True, host='0.0.0.0')
