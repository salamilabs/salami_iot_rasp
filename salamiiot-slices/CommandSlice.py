import time

import SalamiConstants
from communication.subscriber import SalamiSubscriber
from model.message import SalamiMessageBuilder
from model.util import SalamiModelUtil
from slice import ABSSlice

SLICE_SLOT = "MASTER_CHEF"


class CommandSlice(ABSSlice):
    def __init__(self, slice_slot):
        super().__init__(slice_slot)
        self._publisher = SalamiSubscriber(self._passport.socket_url + "/upstream?id=" + SLICE_SLOT,
                                           self.print_response)
        self._publisher.start()
        time.sleep(.1)

    def send_ingredient_update(self):
        builder = SalamiMessageBuilder()
        builder.with_source(SLICE_SLOT)
        builder.with_destination(2)
        builder.with_ingredient(5)
        builder.with_command(SalamiConstants.CMD_DO_GPIO_ACTION)
        action_request = builder.adapt_to().object()
        json_message = SalamiModelUtil.object_to_json(action_request)
        self._publisher.publish(json_message)

    def print_response(self, message):
        self._logger.info("Received message %s", message)


if __name__ == '__main__':
    command_slice = CommandSlice(SLICE_SLOT)
    command_slice.send_ingredient_update()
