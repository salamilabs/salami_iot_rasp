import logging

from decorators import consumes
from model.action import SalamiAction
from slice import ABSSlice

SLICE_SLOT = 5


class SaySlice(ABSSlice):
    def __init__(self, slice_slot):

        super().__init__(slice_slot)
        self.__logger = logging.getLogger(self.__class__.__name__)

    @consumes(SalamiAction)
    def on_message(self, message):
        self.__logger.info("Received message: %s", message)
        self.__logger.info("Action name is: %s", message.name)
        if message.is_message_action():
            self.__logger.info("Payload is: %s", message.get_dataload_field("Text1"))


if __name__ == "__main__":
    say_slice = SaySlice(SLICE_SLOT)
    say_slice.activate()
