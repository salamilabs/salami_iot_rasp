#!/usr/bin/env bash

function start {
   sudo /opt/salamiiot/salami.sh "start" &
	sleep 4s
    sudo /opt/salamiiot/slices.sh "start" "EmailSlice" &
    sudo /opt/salamiiot/slices.sh "start" "AlarmSlice" &
    sudo /opt/salamiiot/slices.sh "start" "ModbusSliceFlask" &
}

function stop {
    sudo /opt/salamiiot/slices.sh "stop"
    sudo /opt/salamiiot/salami.sh "stop"
}

if [ $1 == "start" ]; then
    start
elif [ $1 == "stop" ]; then
    stop
else
    echo "Usage: start_poc.sh [ start - stop ]"
fi
