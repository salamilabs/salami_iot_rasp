#!/usr/bin/env bash

VIRTUAL_ENV_HOME="/opt/salamiiot/salami_venv"
SALAMI_HOME="/opt/salamiiot"

PYTHON_PATH="sudo "${VIRTUAL_ENV_HOME}"/bin/python "

TORNADO_FILE="SliceIOTManager.py"
START_TORNADO_CMD=${PYTHON_PATH}${SALAMI_HOME}"/salamiiot/tornado/"${TORNADO_FILE}

MASTERCHEF_FILE="master.py"
START_MASTERCHEF_CMD=${PYTHON_PATH}${SALAMI_HOME}"/salamiiot/chef/"${MASTERCHEF_FILE}

function start {
    source ${VIRTUAL_ENV_HOME}"/bin/activate"
    cd ${SALAMI_HOME}
    sudo cp -R salamiiot ${VIRTUAL_ENV_HOME}"/lib/python3.4/"
    cd salamiiot
    ${START_TORNADO_CMD} &
    sleep 3s
    if [ $? -eq 0 ]; then
        ${START_MASTERCHEF_CMD} &
        if [ $? -eq 0 ]; then
            echo "Salami started"
        else
            echo "ERROR - Cannot start the MasterChef!"
            exit -2
        fi
    else
        echo "ERROR - Cannot start SliceIOTManager!"
        exit -1
    fi
}

function stop {
    sudo pkill -f ${TORNADO_FILE}
    sudo pkill -f ${MASTERCHEF_FILE}
}

if [ $1 == "start" ]; then
    start
elif [ $1 == "stop" ]; then
    stop
else
    echo "Usage: salami.sh [ start - stop ]"
fi
