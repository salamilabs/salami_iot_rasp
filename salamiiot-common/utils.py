import time


# Function to get the current time in milliseconds
def get_current_milliseconds_time():
    return int(round(time.time() * 1000))


# Fake callback placeholder
class MessageCallbackPlaceholder:
    def __init__(self, message):
        self.__message = message


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
