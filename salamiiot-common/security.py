import hashlib
import logging
import os

import yaml
from Crypto.Cipher import AES

from communication.httpclient import HttpClient
from configuration import ConfigurationManager

MASTERCHEF_PASSPORT_TYPE = 1
SLICE_PASSPORT_TYPE = 2

DATA_FOLDER_PATH = os.path.abspath("/opt/salamiiot/data/")
KEY_FILE_NAME = "key.salami"
PASSPORT_FILE_NAME = "passport.salami"


class SecurityManager:
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._configuration_manager = ConfigurationManager()
        self._key = ""

    def check_key(self):
        key_path = os.path.join(DATA_FOLDER_PATH, KEY_FILE_NAME)
        self._logger.info("Checking Key")

        if os.path.isfile(key_path):
            with open(key_path, 'r') as key_file:
                key = key_file.read().strip()
                if self._verify_remote_key(key):
                    self._key = key
                    self._logger.info("Key correctly verified")
                    return True
        else:
            self._logger.warn("Key not verified!")
            return False

    def check_passport(self, p_type):
        self._logger.info("Checking the passport file")
        passport_path = os.path.join(DATA_FOLDER_PATH, PASSPORT_FILE_NAME)

        if os.path.isfile(passport_path):
            with open(passport_path, 'rb') as passport_file:
                passport = passport_file.read()
                return self._validate_passport(passport, p_type)
        else:
            self._logger.warn("Passport not verified")
            return False

    def generate_passport(self, content, p_type):
        yml_passport = yaml.dump(content, default_flow_style=True)

        config = CipherConfig(self._key, self._configuration_manager.get_configuration_entry('masterchef_id'))
        cipher = SalamiCipher(config)
        enc_passport = cipher.encrypt(yml_passport)

        passport_path = os.path.join(DATA_FOLDER_PATH, PASSPORT_FILE_NAME)
        with open(passport_path, 'wb') as outfile:
            outfile.write(enc_passport)

    def get_passport_entry(self, key):
        return self._passport[key]

    def save_key(self, key):
        self._logger.info("Saving key to te filesystem")
        with open(os.path.join(DATA_FOLDER_PATH, KEY_FILE_NAME), "w") as text_file:
            text_file.write(key)
            self._key = key

    @staticmethod
    def _verify_remote_key(key):
        client = HttpClient("http://127.0.0.1:8000/verify?k=" + key)
        status = client.get().status_only()

        if status is 200:
            return True
        return False

    def _validate_passport(self, passport, p_type):
        config = None
        if p_type == MASTERCHEF_PASSPORT_TYPE:
            config = CipherConfig(self._key, self._configuration_manager.get_configuration_entry('masterchef_id'))
        elif p_type == SLICE_PASSPORT_TYPE:
            config = CipherConfig(self._key, self._configuration_manager.get_configuration_entry('slice_slot'))
        cipher = SalamiCipher(config)
        plain_passport = cipher.decrypt(passport)
        plain_passport = plain_passport.decode("Utf-8").replace(";", "")
        dict_passport = yaml.load(plain_passport)
        if SecurityManager._check_passport_content(dict_passport):
            self._passport = dict_passport
            return True
        return False

    @staticmethod
    def _check_passport_content(passport):
        result = False
        if passport['toi_url'] is not None or passport['toi_url'] is not "":
            result = True
            if passport['socket_url'] is not None or passport['socket_url'] is not "":
                result = True
        return result


class SalamiCipher:
    def __init__(self, config):
        vector = hashlib.sha224(str(config.get_vector()).encode("utf-8")).hexdigest()
        self._aes = AES.new(config.key, AES.MODE_CBC, bytes(vector[:16].encode("utf-8")))

    def encrypt(self, content):
        padding = 160 - len(content)
        content += ';' * padding
        return self._aes.encrypt(content)

    def decrypt(self, content):
        return self._aes.decrypt(content)


class CipherConfig:
    def __init__(self, key, masterchef_id="", slice_slot=0):
        self.key = key
        self.masterchef_id = masterchef_id
        self.slice_slot = slice_slot

    def get_vector(self):
        ordered = [str(value) for value in self.__dict__.values()]
        ordered.sort()
        return ''.join(ordered)
