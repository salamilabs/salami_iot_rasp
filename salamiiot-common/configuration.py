import os

import yaml

CONF_FOLDER_PATH = os.path.abspath("/opt/salamiiot/conf/")
LOGGING_CONF_FILE_PATH = os.path.join(CONF_FOLDER_PATH, "logger.yml")
CONF_FILE_PATH = os.path.join(CONF_FOLDER_PATH, "conf.yml")


class ConfigurationManager:
    def __init__(self):
        self._configuration = None
        with open(CONF_FILE_PATH, 'r') as conf_file:
            self._configuration = yaml.load(conf_file)

        self._logging_configuration = None
        with open(LOGGING_CONF_FILE_PATH, 'r') as logging:
            self._logging_configuration = yaml.load(logging)

    def get_logger_configuration(self, logger):
        return self._logging_configuration["loggers"][logger]

    def get_configuration_entry(self, key):
        return self._configuration['configuration'][key]
