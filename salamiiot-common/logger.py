import logging

import SalamiConstants
from communication.subscriber import SalamiSubscriber
from configuration import ConfigurationManager
from decorators import consumes
from model.loggerdata import FileHandlerData, WsHandlerData, LoggerDataBuilder, HandlerData, Trigger, \
    LoggingConfigurationBuilder
from model.message import SalamiMessage
from utils import Singleton, MessageCallbackPlaceholder

ROOT_LOGGER_NAME = "root"


class SalamiLogger(metaclass=Singleton):
    def __init__(self, socket_url):
        self._socket_url = socket_url

        self.receiver = SalamiSubscriber(self._socket_url + "/system?id=LOG", self.on_message)
        self._configuration = ConfigurationParser().parse()
        self._bootstrap_root_logger()
        self._bootstrap_masterchef_logger()
        self._bootstrap_assistantchef_logger()

    def _bootstrap_root_logger(self):
        root_conf = self._configuration.root_logger
        if root_conf.enabled:
            for handler in root_conf.handlers:
                self._setup_handler(handler, ROOT_LOGGER_NAME)

    def _bootstrap_masterchef_logger(self):
        self._bootstrap_full_logger(self._configuration.masterchef_logger)

    def _bootstrap_assistantchef_logger(self):
        self._bootstrap_full_logger(self._configuration.assistantchef_logger)

    def _bootstrap_full_logger(self, logger_data):
        logger = logging.getLogger(logger_data.id)
        if logger_data.enabled:
            logger.setLevel(logger_data.level)
            for handler in logger_data.handlers:
                self._setup_handler(handler, logger_data.id)
        else:
            logger.addHandler(logging.NullHandler())
        for trigger in logger_data.triggers:
            logger.addFilter(ExcludeFilter(trigger.name))

    @staticmethod
    def _setup_handler(handler, logger_name):
        if logger_name == ROOT_LOGGER_NAME:
            logger = logging.getLogger()
        else:
            logger = logging.getLogger(logger_name)

        if isinstance(handler, FileHandlerData) and handler.enabled:
            file_handler = logging.FileHandler(handler.file_path)
            file_handler.setLevel(handler.level)
            formatter = logging.Formatter(handler.format)
            file_handler.setFormatter(formatter)
            logger.addHandler(file_handler)
        elif isinstance(handler, WsHandlerData) and handler.enabled:
            ws_handler = WebSocketHandler(handler.socket_url, logger_name)
            ws_handler.setLevel(handler.level)
            formatter = logging.Formatter(handler.format)
            ws_handler.setFormatter(formatter)

    @consumes(SalamiMessage)
    def on_message(self, message):
        if message.command == SalamiConstants.CMD_SET_LOGGING:
            # TODO: Check and save the new configuration
            self._bootstrap_root_logger()
            self._bootstrap_masterchef_logger()
            self._bootstrap_assistantchef_logger()

    @staticmethod
    def get_logger(name):
        return logging.getLogger(name)


class ExcludeFilter(logging.Filter):
    def __init__(self, to_exclude):
        super().__init__()
        self.to_exclude = to_exclude

    def filter(self, record):
        return not record.getMessage().startswith(self.to_exclude)


class WebSocketHandler(logging.Handler):
    def __init__(self, socket_url, logger_name):
        super().__init__()
        self._publisher = SalamiSubscriber(socket_url + "/system?id=LOG:" + logger_name,
                                           MessageCallbackPlaceholder)

    def emit(self, record):
        self._publisher.publish(record)


class ConfigurationParser:
    def __init__(self):
        self._configuration = ConfigurationManager()

    def parse(self):
        root_logger_conf = self._configuration.get_logger_configuration(SalamiConstants.ROOT_LOGGER_ID)
        root_logger_data = LoggerDataBuilder().with_id(SalamiConstants.ROOT_LOGGER_ID).enabled(
            root_logger_conf['is_enabled']).with_level(root_logger_conf['level']).root(root_logger_conf['is_root'])
        ConfigurationParser._add_handlers(root_logger_data, root_logger_conf)
        root_logger_data = root_logger_data.adapt_to().object()

        masterchef_logger_conf = self._configuration.get_logger_configuration(SalamiConstants.MASTERCHEF_LOGGER_ID)
        masterchef_logger_data = LoggerDataBuilder().enabled(masterchef_logger_conf['is_enabled']).root(
            masterchef_logger_conf['is_root']).with_id(SalamiConstants.MASTERCHEF_LOGGER_ID)
        ConfigurationParser._add_handlers(masterchef_logger_data, masterchef_logger_conf)
        ConfigurationParser._add_triggers(masterchef_logger_data, masterchef_logger_conf)
        masterchef_logger_data = masterchef_logger_data.adapt_to().object()

        assistantchef_logger_conf = self._configuration.get_logger_configuration(
            SalamiConstants.ASSISTANTCHEF_LOGGER_ID)
        assistantchef_logger_data = LoggerDataBuilder().enabled(assistantchef_logger_conf['is_enabled']).root(
            assistantchef_logger_conf['is_root']).with_id(SalamiConstants.ASSISTANTCHEF_LOGGER_ID)
        ConfigurationParser._add_handlers(assistantchef_logger_data, assistantchef_logger_conf)
        ConfigurationParser._add_triggers(assistantchef_logger_data, assistantchef_logger_conf)
        assistantchef_logger_data = assistantchef_logger_data.adapt_to().object()

        logging_configuration = LoggingConfigurationBuilder().with_root_logger(root_logger_data).with_masterchef_logger(
            masterchef_logger_data).with_assistantchef_logger(assistantchef_logger_data).adapt_to().object()

        return logging_configuration

    @staticmethod
    def _add_handlers(logger_data, logger_conf):
        for handler_conf_label in logger_conf['handlers']:
            handler_conf = logger_conf['handlers'][handler_conf_label]
            handler = HandlerData()
            if handler_conf['type'] == "FileHandler":
                handler = FileHandlerData()
                handler.enabled = handler_conf['is_enabled']
                handler.type = handler_conf['type']
                handler.file_path = handler_conf['file_path']
                handler.format = handler_conf['format']
                handler.level = handler_conf['level']
            if handler_conf['type'] == "WsHandler":
                handler = WsHandlerData()
                handler.enabled = handler_conf['is_enabled']
                handler.type = handler_conf['type']
                handler.socket_url = handler_conf['socket_url']
                handler.format = handler_conf['format']
                handler.level = handler_conf['level']
            logger_data.with_handler(handler)

    @staticmethod
    def _add_triggers(logger_data, logger_conf):
        if 'triggers' in logger_conf:
            for trigger_name in logger_conf['triggers']:
                trigger = Trigger(trigger_name)
                logger_data.with_trigger(trigger)
