import time

from model.util import SalamiModelUtil


def consumes(clazz):
    def decorator(func_to_decorate):
        def wrapped(self, message):
            to_clazz = SalamiModelUtil.json_to_object(message, clazz)
            func_to_decorate(self, to_clazz)

        return wrapped

    return decorator


def synchronized(lock):
    def wrap(f):
        def new_function(*args, **kw):
            lock.acquire()
            try:
                return f(*args, **kw)
            finally:
                lock.release()

        return new_function

    return wrap


def singleton(class_):
    instances = {}

    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]

    return getinstance


def sleep_after(secs):
    def decorator(func_to_decorate):
        def wrapped(*args, **kw):
            func_to_decorate(*args, **kw)
            time.sleep(secs)

        return wrapped

    return decorator
